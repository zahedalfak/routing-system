﻿using System;
using System.Collections.Generic;
using System.Linq;
using RoutingSystems.Model;

namespace RoutingSystems.Controller
{
    class AdminController
    {
        RoutingSystems.Model.rasharoutesystemEntities db = new RoutingSystems.Model.rasharoutesystemEntities(Properties.Settings.Default.cn3);
        public void Insert(String Username,String Password,String name,String lname,String Image)
        {
            Admin admin = new Admin()
            {
                username = Username,
                password = Password,
                name = name,
                lname = lname,
                image = Image
            };
            db.Admins.Add(admin);
            db.SaveChanges();
        }
        public void Delete(int ID)
        {
            var delete = db.Admins.FirstOrDefault(c => c.id == ID);
            db.Admins.Remove(delete);
            db.SaveChanges();
        }
       
        //public void Update(int ID,String Username,String Password,String name,String lname,String Image)
        //{

        //}
        public int Login(String Username,String Password)
        {
            var login = db.Admins.Where(c => c.username == Username && c.password == Password);
            if (login.Any())
            {
                var getId = login.First();
                return getId.id; 
            }
            else
            {
                return 0;
            }
        }
        public void InsertPermissionAdmin(int ID,int PermissionID)
        {

        }
        public void InsertPermissionsAdmin(int ID,List<int> PermissionsID)
        {

        }
        public void DeletePermissionAdmin(int ID,int PermissionID)
        {

        }

        public void DeletePermissionsAdmin(int ID,List<int> PermissionID)
        {

        }

        public void GetPermissionsAdmin(int ID)
        {

        }
        public void CheckPermissionAdmin(int ID,int PermissionID)
        {

        }
    }
}