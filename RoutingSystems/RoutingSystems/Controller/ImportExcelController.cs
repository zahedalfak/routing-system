﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Mvvm.POCO;
using DevExpress.XtraGrid;
using RoutingSystems.Model;

namespace RoutingSystems.Controller
{

    class ImportExcelController
    {
        RoutingSystems.Model.rasharoutesystemEntities db = new RoutingSystems.Model.rasharoutesystemEntities(Properties.Settings.Default.cn3);


        public void InsertCatch(IEnumerable<Customer> entity) {
            db.Customers.AddRange(entity);
            db.SaveChanges();
            
        }

        public int Count()
        {
            return db.Customers.Count();
        }
        public void ShowData(GridControl data)
        {
            data.DataSource = db.Customers.ToList();
        }
        //public int CheckNameBusiness(String name)
        //{
        //    var check = db.Businesses.Where(c => c.name == name);
            
        //    if (check.Any())
        //    {
        //        return check.First().id;
        //    }
        //    else
        //    {
        //        Business business = new Business()
        //        {
        //            name = name
        //        };
        //        db.Businesses.Add(business);
        //        db.SaveChanges();
        //        return db.Businesses.First(c => c.name == name).id;
        //    }
        //}
        //public void CheckSeller(int id,String name)
        //{
        //    var check = db.Sellers.Where(c => c.id == id);
        //    if (check.Any())
        //    {
        //        //
        //    }
        //    else
        //    {
        //        Seller seller = new Seller()
        //        {
        //            id = id,
        //            name = name
        //        };
        //        db.Sellers.Add(seller);
        //        db.SaveChanges();
        //    }
        //}
        //public int CheckActivity(String name)
        //{
        //    var check = db.Activities.Where(c => c.name == name);
        //    if (check.Any())
        //    {
        //       return check.First().id;
        //    }
        //    else
        //    {
        //        db.Activities.Add(new Activity()
        //        {
        //            name = name
        //        });
        //        db.SaveChanges();
        //        return db.Activities.First(c => c.name == name).id;
        //    }
        //}
        //public int CheckMarket(String name)
        //{
        //    var check = db.Markets.Where(c => c.name == name);
        //    if (check.Any())
        //    {
        //        return check.First().id;
        //    }
        //    else
        //    {
        //        db.Markets.Add(new Market()
        //        {
        //            name = name
        //        });
        //        db.SaveChanges();
        //        return db.Markets.First(c => c.name == name).id;
        //    }
        //}
        public int CheckCity(String name)
        {
            var check = db.Cities.Where(c => c.name == name);
            if (check.Any())
            {
                return check.First().id;
            }
            else
            {
                City city = new City()
                {
                    name = name
                };
                db.Cities.Add(city);
                db.SaveChanges();
                return db.Cities.First(c => c.name == name).id;
            }
        }
        //public string CheckRegion(String id,String name,int city)
        //{
        //    var check = db.vRegion.Where(c => c.name == name);
        //    if (check.Any())
        //    {
        //        return check.First().id;
        //    }
        //    else
        //    {
        //        Region region = new Region()
        //        {
        //            id=id,
        //            name = name,
        //            city_id=city
        //        };
        //        db.vRegion.Add(region);
        //        db.SaveChanges();
        //        return db.vRegion.First(c => c.name == name).id;
        //    }
        //}
        public int GetRegionId(String id,String name,int city)
        {
            var get = db.vRegion.Where(c => c.EDMXID == int.Parse(id) && c.RegionName == name && c.CityID == city);
            if (get.Any())
            {
                return get.First().EDMXID;
            }
            else { return 0;  }
        }
        //public string CheckDistract(string id,String name,int city_id,String region_id)
        //{
        //    var check = db.vDistrict.Where(c => c.name == name && c.region_id == region_id);
        //    if (check.Any())
        //    {
        //        return check.First().id;
        //    }
        //    else
        //    {
        //        District district = new District()
        //        {
        //            id=id,
        //            name = name,
        //            city_id = city_id,
        //            region_id= region_id
        //        };
        //        db.vDistrict.Add(district);
        //        db.SaveChanges();
        //        return db.vDistrict.First(c => c.name == name).id;
        //    }
        //}
        public int GetDistractId(String id, String name, int city, String region_id)
        {
            var get = db.vDistrict.Where(c => c.EDMXID == int.Parse(id) && c.DistrictName == name && c.CityID == city && c.Region == region_id);
            if (get.Any())
            {
                return get.First().EDMXID;
            }
            else { return 0; }
        }
        //public string CheckRang(string id,String name,int city_id,string district_id)
        //{
        //    var check = db.vRange.Where(c => c.name == name && c.district_id == district_id);
        //    if (check.Any())
        //    {
        //        return check.First().id;
        //    }
        //    else
        //    {
        //        Range range = new Range()
        //        {
        //            id=id,
        //            name = name,
        //            city_id = city_id,
        //            district_id = district_id
        //        };
        //        db.vRange.Add(range);
        //        db.SaveChanges();
        //        return db.vRange.First(c => c.name == name).id;
        //    }
        //}
        public int GetRangId(String id, String name, int city_id, String district_id)
        {
            var get = db.vRange.Where(c => c.EDMXID == int.Parse(id) && c.RangeName == name && c.CityID == city_id && c.District == district_id);
            if (get.Any())
            {
                return get.First().EDMXID;
            }
            else { return 0; }
        }
        //public string CheckWay(string id,String name,int city_id,string rang_id)
        //{
        //    var check = db.vWay.Where(c => c.name == name && c.rang_id == rang_id);
        //    if (check.Any())
        //    {
        //        return check.First().id;
        //    }
        //    else
        //    {
        //        Way way = new Way()
        //        {
        //            id = id,
        //            name = name,
        //            city_id = city_id,
        //            rang_id = rang_id
        //        };
        //        db.vWay.Add(way);
        //        db.SaveChanges();
        //        return db.vWay.First(c => c.name == name).id;
        //    }
        //}
        public int GetWayId(string id, String name, int city_id, string rang_id)
        {
            var get = db.vWay.Where(c => c.EDMXID == int.Parse(id) && c.WayName == name && c.CityID == city_id && c.Range == rang_id);
            if (get.Any())
            {
                return get.First().EDMXID;
            }
            else { return 0; }
        }
        //public int CheckOrganizational(String name)
        //{
        //    var check = db.Organizationals.Where(c => c.name == name);
        //    if (check.Any())
        //    {
        //        return check.First().id;
        //    }
        //    else
        //    {
        //        db.Organizationals.Add(new Organizational()
        //        {
        //            name = name
        //        });
        //        db.SaveChanges();
        //        return db.Organizationals.First(c => c.name == name).id;
        //    }
        //}        
        //public int CheckCustomerType(String name)
        //{
        //    var check = db.CustomerTypes.Where(c => c.name == name);
        //    if (check.Any())
        //    {
        //        return check.First().id;
        //    }
        //    else
        //    {
        //        db.CustomerTypes.Add(new CustomerType()
        //        {
        //            name = name
        //        });
        //        db.SaveChanges();
        //        return db.CustomerTypes.First(c => c.name == name).id;
        //    }
        //}
        //public int CheckSellCenterCode(String code)
        //{
        //    var check = db.SellCenterCodes.Where(c => c.code == code);
        //    if (check.Any())
        //    {
        //        return check.First().id;
        //    }
        //    else
        //    {
        //        SellCenterCode sellcentercode = new SellCenterCode()
        //        {
        //            code = code
        //        };
        //        db.SellCenterCodes.Add(sellcentercode);
        //        db.SaveChanges();
        //        return db.SellCenterCodes.First(c => c.code == code).id;
        //    }
        //}
        public void Clear()
        {
            //db.();
            //db.SaveChanges();
        }
    }
}
