﻿using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms;
using RoutingSystems.Model;
using RoutingSystems.View;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RoutingSystems.Controller
{
    class RouteController
    {

        /*
        * Route All Points In OSM
        * 
        * 
        * */
        RoutingSystems.Model.rasharoutesystemEntities db = new RoutingSystems.Model.rasharoutesystemEntities(Properties.Settings.Default.cn3);
        public static List<PointLatLng> points = new List<PointLatLng>();
        public static ArrayList ids = new ArrayList();
        public static PointLatLng _start;
        public static List<GMapOverlay> FinalRouts = new List<GMapOverlay>();
        public static void Add(PointLatLng start)
        {
            _start = start;
            RouteController.DeleteAll();
            for (int i = 0; i < points.Count; i++)
            {
                try
                {
                    MapRoute route = OpenStreetMapProvider.Instance.GetRoute(start, points[i], false, false, 15);
                    GMapRoute r = new GMapRoute(route.Points, "Myroutes");
                    GMapOverlay routesOverlay = new GMapOverlay("Myroutes");
                    routesOverlay.Routes.Add(r);
                    RouteController.Add(points[i].ToString(), r.Distance, int.Parse(ids[i].ToString()));
                }catch(Exception ex)
                {

                }
            }
            int id = GetMin();
            int index = ids.IndexOf(id);
            RouteController.AddFinal(_start,points[index]);
            //
            PointLatLng Catch = points[index];
            //
            ids.RemoveAt(index);
            points.RemoveAt(index);
            if(points.Count == 0)
            {
                //For Show Routes
                CallBackMainForAddRoutes f = new Main();
                f.Prepare();
            }
            else
            {
                //First Rmove
                RouteController.Add(Catch);
            }
           
        }
        
        public static void AddFinal(PointLatLng start, PointLatLng end)
        {
            MapRoute route = OpenStreetMapProvider.Instance.GetRoute(start, end, false, false, 15);
            GMapRoute r = new GMapRoute(route.Points, "Myroutes");
            GMapOverlay routesOverlay = new GMapOverlay("Myroutes");
            routesOverlay.Routes.Add(r);
            FinalRouts.Add(routesOverlay);
        }
        public static void Show(GMapControl gmap)
        {
            MessageBox.Show(FinalRouts.Count.ToString());
            for (int i = 0; i < FinalRouts.Count; i++)
            {
                gmap.Overlays.Add(FinalRouts[i]);
            }
            
        }
        public static void Add(String latlong, Double dis,int customer_id)
        {

            RoutingSystems.Model.rasharoutesystemEntities db = new RoutingSystems.Model.rasharoutesystemEntities(Properties.Settings.Default.cn3);
            CacheMap cacheMap = new CacheMap()
            {

                latlong = latlong,
                distance = dis,
                customer_id = customer_id
            };
            db.CacheMaps.Add(cacheMap);
            db.SaveChanges();
        }
        public static void DeleteAll()
        {
            RoutingSystems.Model.rasharoutesystemEntities db = new RoutingSystems.Model.rasharoutesystemEntities(Properties.Settings.Default.cn3);
            var del = db.CacheMaps;
            db.CacheMaps.RemoveRange(del);
            db.SaveChanges();
        }
        public static int GetMin()
        {
            RoutingSystems.Model.rasharoutesystemEntities db = new RoutingSystems.Model.rasharoutesystemEntities(Properties.Settings.Default.cn3);
            var min = db.CacheMaps.OrderBy(c => c.distance);

            return int.Parse( min.First().customer_id.ToString());
        }
       
    }
    
}
