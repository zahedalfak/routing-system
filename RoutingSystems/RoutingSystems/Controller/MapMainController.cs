﻿using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RoutingSystems.Model;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using RoutingSystems.View;
using System.Threading;

namespace RoutingSystems.Controller
{
    class MapMainController
    {
        public static ArrayList id = new ArrayList();
        ArrayList titles = new ArrayList();
        ArrayList info = new ArrayList();
        ArrayList ways = new ArrayList();
        ArrayList lat = new ArrayList();
        ArrayList longv = new ArrayList();
        public static List<PointLatLng> lists = new List<PointLatLng>();
        RoutingSystems.Model.rasharoutesystemEntities db = new RoutingSystems.Model.rasharoutesystemEntities(Properties.Settings.Default.cn3);
        public void Add(int city_id,String Region,String District,String Range,String way_id,string market,int status)
        {
            lists = new List<PointLatLng>();

            lat = new ArrayList();
            longv = new ArrayList();
            id = new ArrayList();
            titles = new ArrayList();
            info = new ArrayList();
            ways = new ArrayList();
            if (status == 100)
            {
                var getdata = db.Customers.Where(c => c.CityID == city_id && c.Region.Contains(Region) &&
                c.District.Contains(District) && c.Range.Contains(Range) && c.Way.Contains(way_id)
                && c.Market.Contains(market)
                ).OrderBy(o => o.Way);
                //MessageBox.Show(getdata.Count().ToString());
                string infoStr = string.Empty;
                foreach (var item in getdata)
                {
                    if (item.Latitude != "0" && item.Longitude != "0")
                    {
                        infoStr = String.Format("کدمشتری:{0} - آدرس:{1} - تلفن:{2}", item.CustomerID, item.Address, item.PhoneNumber);
                        id.Add(item.CustomerID);
                        lat.Add(item.Latitude);
                        longv.Add(item.Longitude);
                        titles.Add(item.CustomerName);
                        info.Add(infoStr);
                        ways.Add(item.Way);
                        lists.Add(new PointLatLng(double.Parse(item.Latitude.ToString()), double.Parse(item.Longitude.ToString())));
                    }
                   
                }
            }
            else
            {
                var getdata = db.Customers.Where(c => c.CityID == city_id && c.Region.Contains(Region) && c.District.Contains(District) && c.Range.Contains(Range) 
                && c.Way.Contains(way_id) && c.Market.Contains(market) && c.Status == status.ToString()).OrderBy(o => o.Way);
                foreach (var item in getdata)
                {
                    if (item.Latitude != "0" && item.Longitude!= "0")
                    {
                        id.Add(item.CustomerID);
                        lat.Add(item.Latitude);
                        longv.Add(item.Longitude);
                        ways.Add(item.Way);
                        double latv = double.Parse(item.Latitude.ToString());
                        double _longv = double.Parse(item.Longitude.ToString());
                        lists.Add(new PointLatLng(latv, _longv));
                    }
                }
            }
        }
        public void Remove(String Region, String District, String Range, String way_id)
        {
            var getdata = db.Customers.Where(c => c.Region.Contains(Region) && c.District.Contains(District) && c.Range.Contains(Range) && c.Way.Contains(way_id));
            foreach (var item in getdata)
            {
                if (item.Latitude != "0" && item.Longitude != "0")
                {
                    id.Remove(item.CustomerID);
                    lat.Remove(item.Latitude);
                    longv.Remove(item.Longitude);
                    double latv = double.Parse(item.Latitude.ToString());
                    double _longv = double.Parse(item.Longitude.ToString());
                    lists.Remove(new PointLatLng(latv, _longv));
                }
            }
        }

        
        public void LoadMarker()
        {
            int countlat = lat.Count;

            //gMapControl1.SetZoomToFitRect(new PointLatLng(double.Parse(lat[0].ToString()), double.Parse(longv[0].ToString());

            //Wait wait = new Wait();
            //wait.Show();
            //Application.DoEvents();

            double latv =0, _longv =0;

            if (hasMarker) RemoveMarkers();
            for (int i = 0; i < countlat; i++)
            {
                latv =  double.Parse( lat[i].ToString());
                _longv = double.Parse(longv[i].ToString());
                //marker.Tag = id[i];
                //marker.ToolTipText = tooltip; 
                hasMarker = true;
                Main.AddPint(latv.ToString(), _longv.ToString(), id[i].ToString(), titles[i].ToString(),info[i].ToString(),ways[i].ToString());
            }

            if (latv != 0 && _longv != 0)
            {
                Main.FlyTo(latv.ToString(),_longv.ToString());
                
            }

            
            //for (int i = 0; i < 5000 * 3; i++) ;

            //wait.Close();
        }

       
        bool hasMarker = false;
        public void RemoveMarkers()
        {
            for (int i = 0; i < lat.Count; i++)
            {
                Main.RemoveMarkers(id[i].ToString());
            }

            hasMarker = false;
        }

        public void GetDataMember(int id, LabelControl name, LabelControl bisiness, LabelControl Saller_id,
            LabelControl saller_name, LabelControl home_number, LabelControl home_number_2, LabelControl phone_number,
            LabelControl phone_number_2, LabelControl national_code, LabelControl postal_code, LabelControl economic_code,
            LabelControl address, LabelControl activity, LabelControl market, LabelControl create_at_Faaliat, LabelControl city,
            LabelControl region, LabelControl distract, LabelControl rang, LabelControl way, LabelControl sell_center_name,
            LabelControl type_customer, LabelControl create_at, LabelControl update_at)
        {
            var get = db.Customers.First(c => c.CustomerID == id.ToString());
            name.Text = get.CustomerName;
            String bissinessname = "";// get//GetBissinessname(int.Parse(get.business_id.ToString()));
            bisiness.Text = bissinessname;
            //Saller_id.Text = get.Seller.id.ToString();
            saller_name.Text = get.SellerName;
            //home_number.Text = get.home_number;
            //home_number_2.Text = get.h;
            phone_number.Text = get.PhoneNumber;
            phone_number_2.Text = get.PhoneNumber2;
            national_code.Text = get.NationalCode;
            postal_code.Text = get.PostalCode;
            economic_code.Text = get.EconomicCode;
            address.Text = get.Address;
            activity.Text = get.Activity;
            market.Text = get.Market;
            //create_at_Faaliat.Text = get.cre;
            //city.Text = get.City.name;
            ///
            //var q1 = db.Regions.First(c => c.base_id == get.Region);
            //region.Text = q1.name;
            //
            //var q2 = db.Districts.First(c => c.id_base == get.distract_code);
            //distract.Text = q2.name;
            //
            //var q3 = db.Ranges.First(c => c.base_id == get.range_code);
            //rang.Text = q3.name;
            ////
            //var q4 = db.Ways.First(c => c.base_id == get.way_code);
            //way.Text = q4.name;
            //region.Text = get.region_code;
            sell_center_name.Text = get.SellerName;
            type_customer.Text = get.Customertype;//.name;
        }

        //public String GetBissinessname(int id)
        //{
        //    try
        //    {
        //        var get = db.Businesses.First(c => c.id == id);
        //        return get.name;
        //    }
        //    catch (Exception ex)
        //    {
        //        return "";
        //    }
        //}
    }
}
