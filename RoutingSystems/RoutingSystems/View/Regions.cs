﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RoutingSystems.Model;
using RoutingSystems.Controller;

namespace RoutingSystems.View
{
    public partial class Regions : DevExpress.XtraEditors.XtraForm
    {
        Array item_city_id;
        RegionsController RegionsController;
        RoutingSystems.Model.rasharoutesystemEntities db = new RoutingSystems.Model.rasharoutesystemEntities(Properties.Settings.Default.cn3);
        public Regions()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if(comboBoxEdit1.Text == "...موردی را انتخاب کنید")
            {

            }
            else
            {
                int index = comboBoxEdit1.SelectedIndex;
                int insert = int.Parse( item_city_id.GetValue(index).ToString());
               // int status = RegionsController.Insert(textEdit1.Text.ToString(), insert, RandomNumber(10000, 99999));
                //if(status == 200)
                //{
                    
                //    RegionsController.GetData(gridControl1);
                //}
                //else
                //{
                //    MessageBox.Show("ثبت نشد");
                //}
            }
        }
        public String RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max).ToString();
        }

        private void Regions_Load(object sender, EventArgs e)
        {
            item_city_id = db.View_1.Select(x => x.id).ToArray();
            comboBoxEdit1.Text = "...موردی را انتخاب کنید";
            comboBoxEdit1.Properties.Items.AddRange(db.View_1.Select(x => x.name).ToArray());
            //
            RegionsController = new RegionsController();
            RegionsController.GetData(gridControl1);
        }

        private void حذفToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                int id = int.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "id").ToString());
                MessageBox.Show(id.ToString());
               // RegionsController.Delete(id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
          
        }
    }
}
