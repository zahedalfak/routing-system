﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace RoutingSystems.View// RashaRoutingSystem
{
    public partial class Setting : DevExpress.XtraEditors.XtraForm
    {
        bool from_main;
        public Setting()
        {
            from_main = false;
            InitializeComponent();
        }

        public Setting(bool fromMain)
        {
            from_main = true;
            InitializeComponent();
        }
        private void checkFields()
        {
            if ( txtUser.Text != string.Empty
                && txtPass.Text != string.Empty
                && txtPort.Text != string.Empty)
            {
                btnResetForm.Enabled = true;
                btnConnectionTest.Enabled = true;
                btnSave.Enabled = true;
            }
            else if ( txtUser.Text != string.Empty
                || txtPass.Text != string.Empty
                || txtPort.Text != string.Empty){
                btnResetForm.Enabled = true;
            }
            else
            {
                btnResetForm.Enabled = false;
                btnConnectionTest.Enabled = false;
                btnSave.Enabled = false;
            }
            
        }

        private void txtServerAddress_TextChanged(object sender, EventArgs e)
        {
            checkFields();
        }

        private void txtUser_TextChanged(object sender, EventArgs e)
        {
            checkFields();
        }

        private void txtPass_TextChanged(object sender, EventArgs e)
        {
            checkFields();
        }

        private void txtPort_TextChanged(object sender, EventArgs e)
        {
            checkFields();
            
        }

        private void btnResetForm_Click(object sender, EventArgs e)
        {
            txtUser.Text = string.Empty;
            txtPass.Text = string.Empty;
            txtPort.Text = string.Empty;
        }

        private void btnConnectionTest_Click(object sender, EventArgs e)
        {
            string connectionString = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};", cmbServer.Text, cmbDatabaseName.Text, txtUser.Text, txtPass.Text);
            try
            {
                SqlHelper helper = new SqlHelper(connectionString);
                if (helper.IsConnection)
                    MessageBox.Show("تست با موفقیت انجام شد.", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Setting_Load(object sender, EventArgs e)
        {
            
            cmbServer.Properties.Items.Add(".");
            cmbServer.Properties.Items.Add("(local)");
            cmbServer.Properties.Items.Add(@".\SQLEXPRESS");
            cmbServer.Properties.Items.Add(string.Format(@"{0}\SQLEXPRESS", Environment.MachineName));
            cmbServer.SelectedIndex = 3;

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string connectionString = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};", cmbServer.Text, cmbDatabaseName.Text, txtUser.Text, txtPass.Text);
            
            try
            {
        
                 SqlHelper helper = new SqlHelper(connectionString);
                if (helper.IsConnection)
                {
                    connectionString = string.Format("metadata=res://*/Model.Model1.csdl|res://*/Model.Model1.ssdl|res://*/Model.Model1.msl;provider=System.Data.SqlClient;provider connection string=';Data Source={0};Initial Catalog={1};user id={2};password={3};multipleactiveresultsets=True;application name=EntityFramework';",
                    cmbServer.Text, cmbDatabaseName.Text, txtUser.Text, txtPass.Text); 

                    //AppSetting setting = new AppSetting();
                    //setting.SaveConnectionString("rasharoutesystemEntities", connectionString/*helper.ConnectionString*/);
                    Properties.Settings.Default.cn3 = connectionString;
                    Properties.Settings.Default.Save();

                    MessageBox.Show("رشته اتصال با موفقیت ذخیره شد.", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Information, (MessageBoxDefaultButton)MessageBoxOptions.RtlReading);
                    
                    if (from_main == false)
                    {
                        //Application.Run(new Main());
                        Main setting = new Main();
                        setting.ShowDialog();
                    }
                    this.Close();
                    //Application.Run(new Main());
                    //Application.Restart();
                }
            }
            catch (Exception ex)
            {
                  MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void GetDatabaseList()
        {
            cmbDatabaseName.Properties.Items.Clear();
            if (txtUser.Text != string.Empty
                && txtPass.Text != string.Empty
                && txtPort.Text != string.Empty)
            {

                // Open connection to the database

                string conString = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};", cmbServer.Text, "rasharoutesystem", txtUser.Text, txtPass.Text);
                using (SqlConnection con = new SqlConnection(conString))
                {
                    try
                    {
                        con.Open();

                        // Set up a command with the given query and associate
                        // this with the current connection.
                        using (SqlCommand cmd = new SqlCommand("SELECT name from sys.databases", con))
                        {
                            using (IDataReader dr = cmd.ExecuteReader())
                            {
                                while (dr.Read())
                                {
                                    cmbDatabaseName.Properties.Items.Add(dr[0].ToString());
                                }
                            }
                        }
                    }
                    catch (Exception ex) {
                        MessageBox.Show(ex.Message);
                    }

                    
                }
            }

            

        }

        private void btnGetDatabaseNmae_Click(object sender, EventArgs e)
        {
            GetDatabaseList();
        }
    }
}