﻿namespace RoutingSystems.View
{
    partial class Classification
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.count_4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.count_3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.count_2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.count_1 = new DevExpress.XtraEditors.LabelControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelControl6);
            this.groupBox1.Controls.Add(this.count_4);
            this.groupBox1.Controls.Add(this.labelControl4);
            this.groupBox1.Controls.Add(this.count_3);
            this.groupBox1.Controls.Add(this.labelControl2);
            this.groupBox1.Controls.Add(this.count_2);
            this.groupBox1.Controls.Add(this.labelControl53);
            this.groupBox1.Controls.Add(this.count_1);
            this.groupBox1.Location = new System.Drawing.Point(272, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(279, 102);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(106, 76);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(162, 13);
            this.labelControl6.TabIndex = 41;
            this.labelControl6.Text = "تعداد مشتریان بدون اطلاعات مکانی";
            // 
            // count_4
            // 
            this.count_4.Location = new System.Drawing.Point(11, 76);
            this.count_4.Name = "count_4";
            this.count_4.Size = new System.Drawing.Size(6, 13);
            this.count_4.TabIndex = 40;
            this.count_4.Text = "0";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(142, 57);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(126, 13);
            this.labelControl4.TabIndex = 41;
            this.labelControl4.Text = "تعداد مشتری بدون موقعیت";
            // 
            // count_3
            // 
            this.count_3.Location = new System.Drawing.Point(11, 57);
            this.count_3.Name = "count_3";
            this.count_3.Size = new System.Drawing.Size(6, 13);
            this.count_3.TabIndex = 40;
            this.count_3.Text = "0";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(59, 38);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(209, 13);
            this.labelControl2.TabIndex = 41;
            this.labelControl2.Text = "تعداد مشتریان بدون اطلاعات مکانی و موقعیت";
            // 
            // count_2
            // 
            this.count_2.Location = new System.Drawing.Point(11, 38);
            this.count_2.Name = "count_2";
            this.count_2.Size = new System.Drawing.Size(6, 13);
            this.count_2.TabIndex = 40;
            this.count_2.Text = "0";
            // 
            // labelControl53
            // 
            this.labelControl53.Location = new System.Drawing.Point(154, 19);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(114, 13);
            this.labelControl53.TabIndex = 41;
            this.labelControl53.Text = "تعداد نیاز به تقسیم بندی";
            this.labelControl53.Click += new System.EventHandler(this.labelControl53_Click);
            // 
            // count_1
            // 
            this.count_1.Location = new System.Drawing.Point(11, 19);
            this.count_1.Name = "count_1";
            this.count_1.Size = new System.Drawing.Size(6, 13);
            this.count_1.TabIndex = 40;
            this.count_1.Text = "0";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.simpleButton3);
            this.groupBox2.Controls.Add(this.simpleButton2);
            this.groupBox2.Controls.Add(this.simpleButton1);
            this.groupBox2.Location = new System.Drawing.Point(12, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(254, 102);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(6, 14);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(242, 23);
            this.simpleButton3.TabIndex = 2;
            this.simpleButton3.Text = "تقسیم بندی دستی";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(6, 71);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(242, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "تقسیم بندی با موقعیت";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(6, 42);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(242, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "تقسیم بندی اتومات";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // Classification
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(563, 115);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Classification";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "تقسیم بندی";
            this.Load += new System.EventHandler(this.Classification_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl count_4;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl count_3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl count_2;
        private DevExpress.XtraEditors.LabelControl labelControl53;
        private DevExpress.XtraEditors.LabelControl count_1;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
    }
}