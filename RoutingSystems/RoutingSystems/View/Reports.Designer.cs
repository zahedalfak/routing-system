﻿namespace RoutingSystems.View
{
    partial class Reports
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.coumtbessiness = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.countactivity = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.countmarket = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.countcity = new DevExpress.XtraEditors.LabelControl();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.countcustomer = new DevExpress.XtraEditors.LabelControl();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelControl7);
            this.groupBox1.Controls.Add(this.coumtbessiness);
            this.groupBox1.Controls.Add(this.labelControl5);
            this.groupBox1.Controls.Add(this.countactivity);
            this.groupBox1.Controls.Add(this.labelControl3);
            this.groupBox1.Controls.Add(this.countmarket);
            this.groupBox1.Controls.Add(this.labelControl1);
            this.groupBox1.Controls.Add(this.countcity);
            this.groupBox1.Controls.Add(this.labelControl53);
            this.groupBox1.Controls.Add(this.countcustomer);
            this.groupBox1.Location = new System.Drawing.Point(12, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(815, 32);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(216, 9);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(73, 13);
            this.labelControl7.TabIndex = 47;
            this.labelControl7.Text = "تعداد بیزینس ها";
            // 
            // coumtbessiness
            // 
            this.coumtbessiness.Location = new System.Drawing.Point(165, 9);
            this.coumtbessiness.Name = "coumtbessiness";
            this.coumtbessiness.Size = new System.Drawing.Size(6, 13);
            this.coumtbessiness.TabIndex = 46;
            this.coumtbessiness.Text = "0";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(356, 9);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(57, 13);
            this.labelControl5.TabIndex = 45;
            this.labelControl5.Text = "تعداد فعالیت";
            // 
            // countactivity
            // 
            this.countactivity.Location = new System.Drawing.Point(295, 9);
            this.countactivity.Name = "countactivity";
            this.countactivity.Size = new System.Drawing.Size(6, 13);
            this.countactivity.TabIndex = 44;
            this.countactivity.Text = "0";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(481, 9);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(68, 13);
            this.labelControl3.TabIndex = 43;
            this.labelControl3.Text = "تعداد مارکت ها";
            // 
            // countmarket
            // 
            this.countmarket.Location = new System.Drawing.Point(425, 9);
            this.countmarket.Name = "countmarket";
            this.countmarket.Size = new System.Drawing.Size(6, 13);
            this.countmarket.TabIndex = 42;
            this.countmarket.Text = "0";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(616, 9);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(65, 13);
            this.labelControl1.TabIndex = 41;
            this.labelControl1.Text = "تعداد شهر ها ";
            // 
            // countcity
            // 
            this.countcity.Location = new System.Drawing.Point(555, 9);
            this.countcity.Name = "countcity";
            this.countcity.Size = new System.Drawing.Size(6, 13);
            this.countcity.TabIndex = 40;
            this.countcity.Text = "0";
            // 
            // labelControl53
            // 
            this.labelControl53.Location = new System.Drawing.Point(746, 9);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(63, 13);
            this.labelControl53.TabIndex = 39;
            this.labelControl53.Text = "تعداد مشتری";
            // 
            // countcustomer
            // 
            this.countcustomer.Location = new System.Drawing.Point(685, 9);
            this.countcustomer.Name = "countcustomer";
            this.countcustomer.Size = new System.Drawing.Size(6, 13);
            this.countcustomer.TabIndex = 38;
            this.countcustomer.Text = "0";
            this.countcustomer.Click += new System.EventHandler(this.countcustomer_Click);
            // 
            // Reports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 420);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Reports";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "گذارشات";
            this.Load += new System.EventHandler(this.Reports_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl coumtbessiness;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl countactivity;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl countmarket;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl countcity;
        private DevExpress.XtraEditors.LabelControl labelControl53;
        private DevExpress.XtraEditors.LabelControl countcustomer;
    }
}