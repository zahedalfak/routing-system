﻿using System;
using System.Data;
using ExcelDataReader;
using System.Windows.Forms;
using System.IO;
using RoutingSystems.Controller;
using System.Threading;
using RoutingSystems.Model;
using System.Collections.Generic;
using System.Linq;

namespace RoutingSystems.View
{
    public partial class ExcelImport : DevExpress.XtraEditors.XtraForm
    {
        public ExcelImport()
        {
            InitializeComponent();
        }
        DataTableCollection tableCollection;
        private void btnbrowse_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog() { Filter = "Excel 97-2003 Workbook |*.xls|Excel Workbook|*.xlsx" })
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    txtfilenamexls.Text = openFileDialog.FileName;
                    using (var stream = File.Open(openFileDialog.FileName, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader reader = ExcelReaderFactory.CreateReader(stream))
                        {
                            DataSet result = reader.AsDataSet(new ExcelDataSetConfiguration()
                            {
                            ConfigureDataTable = (_) => new ExcelDataTableConfiguration() { UseHeaderRow = true }
                            });
                            tableCollection = result.Tables;
                            ComboSheet.Properties.Items.Clear();
                            foreach (DataTable table in tableCollection)
                            ComboSheet.Properties.Items.Add(table.TableName);
                            Btnimport.Enabled = true;
                        }
                    }
                }
            }
        }
        ImportExcelController ImportExcelController;
        private void ExcelImport_Load(object sender, EventArgs e)
        {
            ImportExcelController = new ImportExcelController();
           // ImportExcelController.ShowData(gridControl1);
        }
        DataTable dt;
        Thread thr;
        Thread thr2;
        private void Btnimport_Click(object sender, EventArgs e)
        {
            DataTable _dt = tableCollection[ComboSheet.SelectedItem.ToString()];
            this.dt = _dt;
            int full_count = dt.Rows.Count;
            LabEnd.Text = full_count.ToString();
            BtnReset.Enabled = false;
            BtnSample.Enabled = false;
            btnbrowse.Enabled = false;
            Btnimport.Enabled = false;
            ComboSheet.Enabled = false;
            LoadingController.ShowLoading(splashScreenManager1);
            thr = new Thread(insert);
            thr.Start();
        }
        private void insert()
        {
            city = int.Parse( dt.Rows[0]["City"].ToString());
            if (dt != null)
            {
                var cacheDatas = dt.AsEnumerable().Select(row => new Customer
                {
                    
                    CustomerID = row["CustomerID"].ToString(),
                    CustomerName = row["CustomerName"].ToString(),
                    CustomerBusinessName = row["CustomerBusinessName"].ToString(),
                    SellerCode = row["SellerCode"].ToString(),
                    SellerName = row["SellerName"].ToString(),
                    PhoneNumber = row["PhoneNumber"].ToString(),
                    PhoneNumber2 = row["PhoneNumber2"].ToString(),
                    MobileNumber = row["MobileNumber"].ToString(),
                    NationalCode = row["NationalCode"].ToString(),
                    PostalCode = row["PostalCode"].ToString(),
                    EconomicCode = row["EconomicCode"].ToString(),
                    Address = row["Address"].ToString(),
                    Longitude = row["Longitude"].ToString(),
                    Latitude = row["Latitude"].ToString(),
                    Status = row["Status"].ToString(),
                    Activity = row["Activity"].ToString(),
                    Market = row["Market"].ToString(),
                    Openingdate = row["Openingdate"].ToString(),
                    Region = row["Region"].ToString(),
                    RegionName = row["RegionName"].ToString(),
                    District = row["District"].ToString(),
                    DistrictName = row["DistrictName"].ToString(),
                    Range = row["Range"].ToString(),
                    RangeName = row["RangeName"].ToString(),
                    Way = row["Way"].ToString(),
                    WayName = row["WayName"].ToString(),
                    OrganizationalUnit = row["OrganizationalUnit"].ToString(),
                    Organizationunitname = row["Organizationunitname"].ToString(),
                    Clientclass = row["Clientclass"].ToString(),
                    SalesCenterCode = row["SalesCenterCode"].ToString(),
                    SalesCenterName = row["SalesCenterName"].ToString(),
                    Customertype = row["Customertype"].ToString(),
                    Description = row["Description"].ToString(),
                    CityID = city,
                   
                });

                ImportExcelController.InsertCatch(cacheDatas);
                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    ImportExcelController.InsertCatch(
                //    dt.Rows[i]["CustomerID"].ToString(),
                //    dt.Rows[i]["CustomerName"].ToString(),
                //    dt.Rows[i]["CustomerBusinessName"].ToString(),
                //    dt.Rows[i]["SellerCode"].ToString(),
                //    dt.Rows[i]["SellerName"].ToString(),
                //    dt.Rows[i]["PhoneNumber"].ToString(),
                //    dt.Rows[i]["PhoneNumber2"].ToString(),
                //    dt.Rows[i]["MobileNumber"].ToString(),
                //    dt.Rows[i]["NationalCode"].ToString(),
                //    dt.Rows[i]["PostalCode"].ToString(),
                //    dt.Rows[i]["EconomicCode"].ToString(),
                //    dt.Rows[i]["Address"].ToString(),
                //    dt.Rows[i]["Longitude"].ToString(),
                //    dt.Rows[i]["Latitude"].ToString(),
                //    dt.Rows[i]["Status"].ToString(),
                //    dt.Rows[i]["Activity"].ToString(),
                //    dt.Rows[i]["Market"].ToString(),
                //    dt.Rows[i]["Openingdate"].ToString(),
                //    dt.Rows[i]["Region"].ToString(),
                //    dt.Rows[i]["RegionName"].ToString(),
                //    dt.Rows[i]["District"].ToString(),
                //    dt.Rows[i]["DistrictName"].ToString(),
                //    dt.Rows[i]["Range"].ToString(),
                //    dt.Rows[i]["RangeName"].ToString(),
                //    dt.Rows[i]["Way"].ToString(),
                //    dt.Rows[i]["WayName"].ToString(),
                //    dt.Rows[i]["OrganizationalUnit"].ToString(),
                //    dt.Rows[i]["Organizationunitname"].ToString(),
                //    dt.Rows[i]["Clientclass"].ToString(),
                //    dt.Rows[i]["SalesCenterCode"].ToString(),
                //    dt.Rows[i]["SalesCenterName"].ToString(),
                //    dt.Rows[i]["Customertype"].ToString(),
                //    dt.Rows[i]["Description"].ToString()
                //        );
                //    this.Invoke(new Action(() => end()));
                //}
                this.Invoke(new Action(() => EndAll()));
            }
        }
        public void end()
        {
            LabEnd.Text = ImportExcelController.Count().ToString();
        }
        public void EndAll()
        {
            LoadingController.HideLoading(splashScreenManager1);
            ImportExcelController.ShowData(gridControl1);
            thr.Abort();
            BtnReset.Enabled = true;
            BtnSample.Enabled = true;
            btnbrowse.Enabled = true;
            Btnimport.Enabled = true;
            ComboSheet.Enabled = true;
        }
        int city = 0;
        int count;
        RoutingSystems.Model.rasharoutesystemEntities db = new RoutingSystems.Model.rasharoutesystemEntities(Properties.Settings.Default.cn3);
        private void BtnSaveData_Click(object sender, EventArgs e)
        {
            //  city = int.Parse(textBox1.Text);
            LoadingController.ShowLoading(splashScreenManager1);
             count = gridView1.RowCount;
            
            progressBarControl1.Properties.Maximum = count;
            progressBarControl1.Properties.Minimum = 0;
            //groupControl1.Visible = false;
            //int contfull = gridView1.RowCount;
            //17432 / 5 ;
            //
            this.Invoke(new Action(() => end()));
            thr2 = new Thread(test);
            thr2.Start();
            //LoadingController.HideLoading(splashScreenManager1);
        }
        public void test()
        {
            //var businessList = (from row in db.CacheDatas
            //                    select row.CustomerBusinessName).Distinct();



            var customerList = (from row in db.Customers
                                select new
                                {
                                    id = row.SellerCode,
                                    fullname = row.CustomerName,
                                    business_id = row.CustomerBusinessName,
                                    seller_id = row.SellerCode,
                                    home_number = row.PhoneNumber,
                                    home_number_2 = row.PhoneNumber2,
                                    phone_number = row.MobileNumber,
                                    phone_number_2 = "",
                                    national_code = row.NationalCode,
                                    postal_code = row.PostalCode,
                                    economic_code = row.EconomicCode,
                                    address = row.Address,
                                    lat = row.Latitude,
                                    @long = row.Longitude,
                                    status1 = row.Status,
                                    Activity = row.Activity,
                                    market_id = row.Market,
                                    create_at_persian = row.Openingdate,
                                    create_at_england = row.Openingdate,
                                    city_id = city,
                                    city_code = city,
                                    region_id = row.Region,
                                    region_code = row.Region,
                                    distract_id = row.District,
                                    distract_code = row.District,
                                    rang_id = row.Range,
                                    range_code = row.Range,
                                    way_id = row.Way,
                                    way_code = row.Way,
                                    classs = row.Clientclass,
                                    sell_center_code = row.SalesCenterCode,
                                    sell_center_name = row.SalesCenterName,
                                    customer_type_id = row.Customertype,
                                    description = row.Description,
                                    create_at = DateTime.Now,
                                    update_at = DateTime.Now,
                                    Organizational_id = row.Organizationunitname,
                                    admin_insert_id = 1
                                });//.ToList().Select(row => new Customer
        //                        {

        //                            id = string.IsNullOrEmpty(row.id) ? (int) 0: int.Parse(row.id),
        //                            fullname = row.fullname,
        //                            business_id = 28723,//CheckNameBusiness(row.business_id),
        //                            seller_id = string.IsNullOrEmpty(row.seller_id )? 9999999 : int.Parse(row.seller_id),
        //                            home_number = row.home_number,
        //                            home_number_2 = row.home_number_2,
        //                            phone_number = row.phone_number,
        //                            phone_number_2 = "",
        //                            national_code = row.national_code,
        //                            postal_code = row.postal_code,
        //                            economic_code = row.economic_code,
        //                            address = row.address,
        //                            lat = string.IsNullOrEmpty(row.lat) ? (double)0 : double.Parse(row.lat),
        //                            @long = string.IsNullOrEmpty(row.@long) ? (double)0 : double.Parse(row.@long),
        //                            status = row.status1 == "غیرفعال" ? (byte)0 : (byte)1,
        //                            activity_id = 1054,// ImportExcelController.CheckActivity(row.Activity) ,//== string.Empty ? (int)0 : int.Parse(row.Activity),
        //                            market_id = 1090,//ImportExcelController.CheckMarket(row.market_id), // == string.Empty ? (int)0 : int.Parse(row.market_id),
        //                            create_at_persian = row.create_at_persian,
        //                            create_at_england = shamsi2miladi.shamsitomiladi(row.create_at_england),
        //                            city_id = row.city_id,
        //                            city_code = row.city_code ,
        //                            region_id = row.region_id,
        //                            region_code = string.IsNullOrEmpty(row.region_code) ? (int)0 : int.Parse(row.region_code),
        //                            distract_id = row.distract_id,
        //                            distract_code = string.IsNullOrEmpty(row.distract_code) ? (int)0 : int.Parse(row.distract_code),
        //                            rang_id = row.rang_id,
        //                            range_code = string.IsNullOrEmpty(row.range_code) ? (int)0 : int.Parse(row.range_code),
        //                            way_id = row.way_id,
        //                            way_code = string.IsNullOrEmpty(row.way_code) ? (int)0 : int.Parse(row.way_code),
        //                            classs = row.classs,
        //                            sell_center_code = string.IsNullOrEmpty(row.sell_center_code) ? (int)0 : int.Parse(row.sell_center_code),
        //                            sell_center_name = row.sell_center_name,
        //                            customer_type_id = 1007,//ImportExcelController.CheckCustomerType(row.customer_type_id),// == string.Empty ? (int)0 : int.Parse(row.customer_type_id),
        //                            description = row.description,
        //                            create_at = DateTime.Now,
        //                            update_at = DateTime.Now,
        //                            Organizational_id = 1005,//ImportExcelController.CheckOrganizational(row.Organizational_id),// == string.Empty ? (int)0 : int.Parse(row.Organizational_id),
        //                            admin_insert_id = 1,
                                    
        //});

            //var customerList = db.CacheDatas.Select(row => new Customer
            //{
                
            //    id = int.Parse(row.SellerCode),
            //    fullname = row.CustomerName,
            //    business_id = ImportExcelController.CheckNameBusiness(row.CustomerBusinessName),
            //    seller_id = row.SellerCode == string.Empty? 9999999 : int.Parse(row.SellerCode),
            //    home_number = row.PhoneNumber,
            //    home_number_2 = row.PhoneNumber2,
            //    phone_number = row.MobileNumber,
            //    phone_number_2 = "",
            //    national_code = row.NationalCode,
            //    postal_code = row.PostalCode,
            //    economic_code = row.EconomicCode,
            //    address = row.Address,
            //    lat = row.Latitude == string.Empty ? (double) 0 : double.Parse(row.Latitude),
            //    @long = row.Longitude == string.Empty ? (double) 0 : double.Parse(row.Longitude),
            //    status = row.Status == string.Empty ? (byte)0: byte.Parse(row.Status),
            //    activity_id = row.Activity == string.Empty ? (int)0 : int.Parse(row.Activity),
            //    market_id = row.Market == string.Empty ? (int)0 : int.Parse(row.Market),
            //    create_at_persian = row.Openingdate,
            //    create_at_england = shamsi2miladi.shamsitomiladi(row.Openingdate),
            //    city_id = city,
            //    city_code = city,
            //    region_id = row.Region,
            //    region_code = row.Region == string.Empty ? (int)0 : int.Parse(row.Region),
            //    distract_id = row.District,
            //    distract_code = row.District == string.Empty ? (int)0 : int.Parse(row.District),
            //    rang_id = row.Range,
            //    range_code = row.Range == string.Empty ? (int) 0 : int.Parse(row.Range),
            //    way_id = row.Way,
            //    way_code = row.Way == string.Empty ? (int) 0 : int.Parse(row.Way),
            //    classs = row.Clientclass,
            //    sell_center_code = row.SalesCenterCode == string.Empty ? (int)0 : int.Parse(row.SalesCenterCode),
            //    sell_center_name = row.SalesCenterName,
            //    customer_type_id = row.Customertype == string.Empty ? (int)0: int.Parse(row.Customertype),
            //    description = row.Description,
            //    create_at = DateTime.Now,
            //    update_at = DateTime.Now,
            //    Organizational_id = row.Organizationunitname == string.Empty ? (int)0 : int.Parse(row.Organizationunitname),
            //    admin_insert_id = 1
            //}).ToList();

            //db.Customers.AddRange(customerList);
            //db.SaveChanges();
            
            //int count = gridView1.RowCount;

            //for (int i = 0; i < count; i++)
            //{
            //    int id = int.Parse(gridView1.GetRowCellValue(i, "CustomerID").ToString());
            //    String fullname = gridView1.GetRowCellValue(i, "CustomerName").ToString();
            //    int business_id = ImportExcelController.CheckNameBusiness(gridView1.GetRowCellValue(i, "CustomerBusinessName").ToString());
            //    int seller_id;
            //    if (gridView1.GetRowCellValue(i, "SellerCode").ToString() == "")
            //    {
            //        seller_id = 9999999;
            //    }
            //    else
            //    {
            //        seller_id = int.Parse(gridView1.GetRowCellValue(i, "SellerCode").ToString());
            //        ImportExcelController.CheckSeller(int.Parse(gridView1.GetRowCellValue(i, "SellerCode").ToString()), gridView1.GetRowCellValue(i, "SellerName").ToString());
            //    }
            //    String home_number = gridView1.GetRowCellValue(i, "PhoneNumber").ToString();
            //    String home_number_2 = gridView1.GetRowCellValue(i, "PhoneNumber2").ToString();
            //    String phone_number = gridView1.GetRowCellValue(i, "MobileNumber").ToString();
            //    String phone_number_2 = "";
            //    String national_code = gridView1.GetRowCellValue(i, "NationalCode").ToString();
            //    String postal_code = gridView1.GetRowCellValue(i, "PostalCode").ToString();
            //    String economic_code = gridView1.GetRowCellValue(i, "EconomicCode").ToString();
            //    String address = gridView1.GetRowCellValue(i, "Address").ToString();
            //    //Latitude 
            //    Double lat_value = Double.Parse(gridView1.GetRowCellValue(i, "Latitude").ToString());
            //    Double long_value = Double.Parse(gridView1.GetRowCellValue(i, "Longitude").ToString());
            //    String status = gridView1.GetRowCellValue(i, "Status").ToString();
            //    byte j = 1;
            //    if (status == "فعال")
            //    {
            //        j = 1;
            //        status = "true";
            //    }
            //    else
            //    {
            //        j = 0;
            //        status = "false";
            //    }
            //    //bool st = Boolean.Parse(status);
            //    int activity_id = ImportExcelController.CheckActivity(gridView1.GetRowCellValue(i, "Activity").ToString());
            //    int market_id = ImportExcelController.CheckMarket(gridView1.GetRowCellValue(i, "Market").ToString());
            //    String create_at_persian = gridView1.GetRowCellValue(i, "Openingdate").ToString();
            //    int city_id = city;
            //    string region_id = ImportExcelController.CheckRegion(gridView1.GetRowCellValue(i, "Region").ToString(), gridView1.GetRowCellValue(i, "RegionName").ToString(), city);
            //    int region_code = ImportExcelController.GetRegionId(gridView1.GetRowCellValue(i, "Region").ToString(), gridView1.GetRowCellValue(i, "RegionName").ToString(), city);
            //    string distract_id = ImportExcelController.CheckDistract(gridView1.GetRowCellValue(i, "District").ToString(), gridView1.GetRowCellValue(i, "DistrictName").ToString(), city, region_id);
            //    int distract_code = ImportExcelController.GetDistractId(gridView1.GetRowCellValue(i, "District").ToString(), gridView1.GetRowCellValue(i, "DistrictName").ToString(), city, region_id);
            //    string rang_id = ImportExcelController.CheckRang(gridView1.GetRowCellValue(i, "Range").ToString(), gridView1.GetRowCellValue(i, "RangeName").ToString(), city, distract_id);
            //    int rang_code = ImportExcelController.GetRangId(gridView1.GetRowCellValue(i, "Range").ToString(), gridView1.GetRowCellValue(i, "RangeName").ToString(), city, distract_id);
            //    string way_id = ImportExcelController.CheckWay(gridView1.GetRowCellValue(i, "Way").ToString(), gridView1.GetRowCellValue(i, "WayName").ToString(), city_id, rang_id);
            //    int way_code = ImportExcelController.GetWayId(gridView1.GetRowCellValue(i, "Way").ToString(), gridView1.GetRowCellValue(i, "WayName").ToString(), city_id, rang_id);
            //    int city_code = city;
            //    String classs = gridView1.GetRowCellValue(i, "Clientclass").ToString();
            //    int sell_center_code = ImportExcelController.CheckSellCenterCode(gridView1.GetRowCellValue(i, "SalesCenterCode").ToString());
            //    String sell_center_name = gridView1.GetRowCellValue(i, "SalesCenterName").ToString();
            //    int customer_type_id = ImportExcelController.CheckCustomerType(gridView1.GetRowCellValue(i, "Customertype").ToString());
            //    String description = gridView1.GetRowCellValue(i, "Description").ToString();
            //    int Organizational_id = ImportExcelController.CheckOrganizational(gridView1.GetRowCellValue(i, "Organizationunitname").ToString());
            //    Customer customer = new Customer()
            //    {
            //        id = id,
            //        fullname = fullname,
            //        business_id = business_id,
            //        seller_id = seller_id,
            //        home_number = home_number,
            //        home_number_2 = home_number_2,
            //        phone_number = phone_number,
            //        phone_number_2 = phone_number_2,
            //        national_code = national_code,
            //        postal_code = postal_code,
            //        economic_code = economic_code,
            //        address = address,
            //        lat = lat_value,
            //        @long = long_value,
            //        status = j,
            //        activity_id = activity_id,
            //        market_id = market_id,
            //        create_at_persian = create_at_persian,
            //        create_at_england = shamsi2miladi.shamsitomiladi(create_at_persian),
            //        city_id = city_id,
            //        city_code = city_code,
            //        region_id = region_id,
            //        region_code = region_code,
            //        distract_id = distract_id,
            //        distract_code = distract_code,
            //        rang_id = rang_id,
            //        range_code = rang_code,
            //        way_id = way_id,
            //        way_code = way_code,
            //        classs = classs,
            //        sell_center_code = sell_center_code,
            //        sell_center_name = sell_center_name,
            //        customer_type_id = customer_type_id,
            //        description = description,
            //        create_at = DateTime.Now,
            //        update_at = DateTime.Now,
            //        Organizational_id = Organizational_id,
            //        admin_insert_id = 1
            //    };
            //    db.Customers.Add(customer);
            //    db.SaveChanges();
            //    //this.Invoke(new Action(() => SetValue()));
            //}
            //this.Invoke(new Action(() => Finish()));
        }

        int CheckNameBusiness(String name)
        {
            //var check = db.Businesses.Where(c => c.name == name);
            
            this.Invoke(new Action(() => SetValue()));
            return 28723;
            //if (check.Any())
            //{
            //    return check.First().id;
            //}
            //else
            //{

            //    db.Businesses.Add(new Business()
            //    {
            //        name = name
            //    });
            //    db.SaveChanges();
            //    return db.Businesses.First(c => c.name == name).id;
            //}
        }
        private void SetValue()
        {
            int step = progressBarControl1.Properties.Step;
            int stepplus = step + 1;
            progressBarControl1.Properties.Step = stepplus;
            LabStart.Text = stepplus.ToString();
        }
        private void Finish()
        {
            ImportExcelController.Clear();
            ImportExcelController.ShowData(gridControl1);

        }
        private void BtnReset_Click(object sender, EventArgs e)
        {
            ImportExcelController.Clear();
            ImportExcelController.ShowData(gridControl1);
            LabStart.Text = "0";
            LabEnd.Text = "0";
            ComboSheet.Text = "";
            progressBarControl1.Properties.Step = 0;
        }
        private void BtnSample_Click(object sender, EventArgs e)
        {
            FileInfo fi = new FileInfo(Application.StartupPath + "\\Data\\Sample.xls");
            if (fi.Exists)
            {
                System.Diagnostics.Process.Start(Application.StartupPath + "\\Data\\Sample.xls");
            }
            else
            {
                //file doesn't exist
            }
        }
    }
}