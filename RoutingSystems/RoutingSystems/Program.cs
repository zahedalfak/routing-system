﻿using CefSharp;
using CefSharp.WinForms;
using RoutingSystems.View;
using System;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace RoutingSystems
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (Properties.Settings.Default.cn3 == string.Empty)
            {
                Application.Run(new Setting());
            }
            else
                Application.Run(new Main());
        }
    }
}
