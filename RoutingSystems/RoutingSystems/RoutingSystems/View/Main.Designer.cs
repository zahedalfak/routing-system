﻿namespace RoutingSystems.View
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            DevExpress.XtraBars.Docking2010.Views.Tabbed.DockingContainer dockingContainer5 = new DevExpress.XtraBars.Docking2010.Views.Tabbed.DockingContainer();
            this.documentGroup1 = new DevExpress.XtraBars.Docking2010.Views.Tabbed.DocumentGroup(this.components);
            this.document2 = new DevExpress.XtraBars.Docking2010.Views.Tabbed.Document(this.components);
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.applicationMenu1 = new DevExpress.XtraBars.Ribbon.ApplicationMenu(this.components);
            this.BtnClose = new DevExpress.XtraBars.BarButtonItem();
            this.BarShowMap = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.BtnMapDB = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.BarTxtSizeMap = new DevExpress.XtraBars.BarStaticItem();
            this.barToggleSwitchdrag = new DevExpress.XtraBars.BarToggleSwitchItem();
            this.barToggleSwitchscroll = new DevExpress.XtraBars.BarToggleSwitchItem();
            this.barToggleSwitchpolygon = new DevExpress.XtraBars.BarToggleSwitchItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemZoomTrackBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.BarBtnImportForm = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.گ = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.menu_region = new DevExpress.XtraBars.BarButtonItem();
            this.menu_distract = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem10 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage3 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage4 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup8 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup9 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemSpinEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemSpinEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.repositoryItemZoomTrackBar2 = new DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel5 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel5_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.dockPanel4 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel4_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.tabPane1 = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabNavigationPage1 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.labelControl58 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl57 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl56 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl55 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl54 = new DevExpress.XtraEditors.LabelControl();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxEdit5 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl53 = new DevExpress.XtraEditors.LabelControl();
            this.countfilter = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEdit4 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit3 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.tabNavigationPage2 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.نمایشمسیرToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.تستToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabNavigationPage3 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.dockPanel3 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel3_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.update_at = new DevExpress.XtraEditors.LabelControl();
            this.way = new DevExpress.XtraEditors.LabelControl();
            this.sell_center_name = new DevExpress.XtraEditors.LabelControl();
            this.type_customer = new DevExpress.XtraEditors.LabelControl();
            this.create_at = new DevExpress.XtraEditors.LabelControl();
            this.create_at_Faaliat = new DevExpress.XtraEditors.LabelControl();
            this.city = new DevExpress.XtraEditors.LabelControl();
            this.region = new DevExpress.XtraEditors.LabelControl();
            this.distract = new DevExpress.XtraEditors.LabelControl();
            this.rang = new DevExpress.XtraEditors.LabelControl();
            this.postal_code = new DevExpress.XtraEditors.LabelControl();
            this.economic_code = new DevExpress.XtraEditors.LabelControl();
            this.address = new DevExpress.XtraEditors.LabelControl();
            this.market = new DevExpress.XtraEditors.LabelControl();
            this.activity = new DevExpress.XtraEditors.LabelControl();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.national_code = new DevExpress.XtraEditors.LabelControl();
            this.phone_number_2 = new DevExpress.XtraEditors.LabelControl();
            this.phone_number = new DevExpress.XtraEditors.LabelControl();
            this.home_number_2 = new DevExpress.XtraEditors.LabelControl();
            this.home_number = new DevExpress.XtraEditors.LabelControl();
            this.Saller_id = new DevExpress.XtraEditors.LabelControl();
            this.saller_name = new DevExpress.XtraEditors.LabelControl();
            this.bisiness = new DevExpress.XtraEditors.LabelControl();
            this.name = new DevExpress.XtraEditors.LabelControl();
            this.customer_id = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dockPanel2 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel2_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.documentManager1 = new DevExpress.XtraBars.Docking2010.DocumentManager(this.components);
            this.tabbedView1 = new DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView(this.components);
            this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::RoutingSystems.View.Wait), true, true);
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.documentGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.document2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel5.SuspendLayout();
            this.dockPanel5_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.dockPanel4.SuspendLayout();
            this.dockPanel4_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).BeginInit();
            this.tabPane1.SuspendLayout();
            this.tabNavigationPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).BeginInit();
            this.tabNavigationPage2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.tabNavigationPage3.SuspendLayout();
            this.dockPanel3.SuspendLayout();
            this.dockPanel3_Container.SuspendLayout();
            this.dockPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.documentManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // documentGroup1
            // 
            this.documentGroup1.Items.AddRange(new DevExpress.XtraBars.Docking2010.Views.Tabbed.Document[] {
            this.document2});
            // 
            // document2
            // 
            this.document2.Caption = "نقشه";
            this.document2.ControlName = "dockPanel2";
            this.document2.FloatLocation = new System.Drawing.Point(646, 146);
            this.document2.FloatSize = new System.Drawing.Size(852, 358);
            this.document2.Properties.AllowClose = DevExpress.Utils.DefaultBoolean.True;
            this.document2.Properties.AllowFloat = DevExpress.Utils.DefaultBoolean.True;
            this.document2.Properties.AllowFloatOnDoubleClick = DevExpress.Utils.DefaultBoolean.True;
            // 
            // ribbon
            // 
            this.ribbon.ApplicationButtonDropDownControl = this.applicationMenu1;
            this.ribbon.AutoHideEmptyItems = true;
            this.ribbon.AutoSizeItems = true;
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.ribbon.SearchEditItem,
            this.BtnClose,
            this.BarShowMap,
            this.barButtonItem2,
            this.barStaticItem1,
            this.barButtonItem4,
            this.BtnMapDB,
            this.barStaticItem2,
            this.barStaticItem3,
            this.barStaticItem4,
            this.BarTxtSizeMap,
            this.barToggleSwitchdrag,
            this.barToggleSwitchscroll,
            this.barToggleSwitchpolygon,
            this.barEditItem1,
            this.barButtonItem1,
            this.BarBtnImportForm,
            this.barButtonItem3,
            this.barButtonItem5,
            this.barButtonItem7,
            this.گ,
            this.barButtonItem8,
            this.menu_region,
            this.menu_distract,
            this.barButtonItem9,
            this.barButtonItem10,
            this.barButtonItem6});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 255;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1,
            this.ribbonPage2,
            this.ribbonPage3,
            this.ribbonPage4});
            this.ribbon.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemZoomTrackBar1,
            this.repositoryItemSpinEdit1,
            this.repositoryItemSpinEdit2,
            this.repositoryItemSpinEdit3,
            this.repositoryItemSpinEdit4,
            this.repositoryItemImageComboBox1,
            this.repositoryItemLookUpEdit1,
            this.repositoryItemComboBox1,
            this.repositoryItemProgressBar1,
            this.repositoryItemZoomTrackBar2});
            this.ribbon.ShowItemCaptionsInPageHeader = true;
            this.ribbon.Size = new System.Drawing.Size(1228, 130);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.TransparentEditorsMode = DevExpress.Utils.DefaultBoolean.True;
            this.ribbon.Click += new System.EventHandler(this.ribbon_Click);
            // 
            // applicationMenu1
            // 
            this.applicationMenu1.MenuDrawMode = DevExpress.XtraBars.MenuDrawMode.LargeImagesText;
            this.applicationMenu1.Name = "applicationMenu1";
            this.applicationMenu1.Ribbon = this.ribbon;
            // 
            // BtnClose
            // 
            this.BtnClose.Caption = "خـــروج";
            this.BtnClose.Id = 1;
            this.BtnClose.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnClose.ImageOptions.Image")));
            this.BtnClose.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("BtnClose.ImageOptions.LargeImage")));
            this.BtnClose.Name = "BtnClose";
            this.BtnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnClose_ItemClick);
            // 
            // BarShowMap
            // 
            this.BarShowMap.Caption = "نمایش نقشه";
            this.BarShowMap.Id = 2;
            this.BarShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BarShowMap.ImageOptions.Image")));
            this.BarShowMap.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("BarShowMap.ImageOptions.LargeImage")));
            this.BarShowMap.Name = "BarShowMap";
            this.BarShowMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BarShowMap_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "تنظیمات";
            this.barButtonItem2.Id = 3;
            this.barButtonItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.ImageOptions.Image")));
            this.barButtonItem2.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.ImageOptions.LargeImage")));
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barStaticItem1.Id = 5;
            this.barStaticItem1.Name = "barStaticItem1";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "نمایش مشتریان";
            this.barButtonItem4.Id = 6;
            this.barButtonItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.ImageOptions.Image")));
            this.barButtonItem4.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.ImageOptions.LargeImage")));
            this.barButtonItem4.Name = "barButtonItem4";
            this.barButtonItem4.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem4_ItemClick);
            // 
            // BtnMapDB
            // 
            this.BtnMapDB.Id = 7;
            this.BtnMapDB.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnMapDB.ImageOptions.Image")));
            this.BtnMapDB.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("BtnMapDB.ImageOptions.LargeImage")));
            this.BtnMapDB.Name = "BtnMapDB";
            this.BtnMapDB.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnMapDB_ItemClick);
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barStaticItem2.Caption = "نسخه 0.1 - آزمایشی";
            this.barStaticItem2.Id = 8;
            this.barStaticItem2.Name = "barStaticItem2";
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barStaticItem3.Caption = "|";
            this.barStaticItem3.Id = 9;
            this.barStaticItem3.Name = "barStaticItem3";
            // 
            // barStaticItem4
            // 
            this.barStaticItem4.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barStaticItem4.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.barStaticItem4.Caption = "حجم پایگاه داده نقشه : ";
            this.barStaticItem4.Id = 10;
            this.barStaticItem4.Name = "barStaticItem4";
            // 
            // BarTxtSizeMap
            // 
            this.BarTxtSizeMap.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.BarTxtSizeMap.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.BarTxtSizeMap.AllowRightClickInMenu = false;
            this.BarTxtSizeMap.Caption = "....";
            this.BarTxtSizeMap.Id = 11;
            this.BarTxtSizeMap.Name = "BarTxtSizeMap";
            // 
            // barToggleSwitchdrag
            // 
            this.barToggleSwitchdrag.BindableChecked = true;
            this.barToggleSwitchdrag.Caption = "امکان جابجایی نقشه";
            this.barToggleSwitchdrag.Checked = true;
            this.barToggleSwitchdrag.Id = 12;
            this.barToggleSwitchdrag.Name = "barToggleSwitchdrag";
            this.barToggleSwitchdrag.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barToggleSwitchdrag_CheckedChanged);
            // 
            // barToggleSwitchscroll
            // 
            this.barToggleSwitchscroll.BindableChecked = true;
            this.barToggleSwitchscroll.Caption = "امکان اسکرول موس";
            this.barToggleSwitchscroll.Checked = true;
            this.barToggleSwitchscroll.Id = 13;
            this.barToggleSwitchscroll.Name = "barToggleSwitchscroll";
            this.barToggleSwitchscroll.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barToggleSwitchscroll_CheckedChanged);
            // 
            // barToggleSwitchpolygon
            // 
            this.barToggleSwitchpolygon.BindableChecked = true;
            this.barToggleSwitchpolygon.Caption = "نمایش پولیگان";
            this.barToggleSwitchpolygon.Checked = true;
            this.barToggleSwitchpolygon.Id = 14;
            this.barToggleSwitchpolygon.Name = "barToggleSwitchpolygon";
            this.barToggleSwitchpolygon.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barToggleSwitchpolygon_CheckedChanged);
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "زوم نقشه";
            this.barEditItem1.CaptionAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.barEditItem1.Edit = this.repositoryItemZoomTrackBar1;
            this.barEditItem1.EditWidth = 150;
            this.barEditItem1.Id = 15;
            this.barEditItem1.Name = "barEditItem1";
            this.barEditItem1.EditValueChanged += new System.EventHandler(this.barEditItem1_EditValueChanged);
            this.barEditItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barEditItem1_ItemClick);
            // 
            // repositoryItemZoomTrackBar1
            // 
            this.repositoryItemZoomTrackBar1.Maximum = 18;
            this.repositoryItemZoomTrackBar1.Name = "repositoryItemZoomTrackBar1";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "لیست مشتریان";
            this.barButtonItem1.Id = 16;
            this.barButtonItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.Image")));
            this.barButtonItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.LargeImage")));
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // BarBtnImportForm
            // 
            this.BarBtnImportForm.Caption = "ورود اطلاعات از اکسل";
            this.BarBtnImportForm.Id = 17;
            this.BarBtnImportForm.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BarBtnImportForm.ImageOptions.Image")));
            this.BarBtnImportForm.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("BarBtnImportForm.ImageOptions.LargeImage")));
            this.BarBtnImportForm.Name = "BarBtnImportForm";
            this.BarBtnImportForm.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.BarBtnImportForm.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BarBtnImportForm_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "لیست شهر ها";
            this.barButtonItem3.Id = 19;
            this.barButtonItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.ImageOptions.Image")));
            this.barButtonItem3.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.ImageOptions.LargeImage")));
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "barButtonItem5";
            this.barButtonItem5.Id = 246;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "تقسیم بندی";
            this.barButtonItem7.Id = 247;
            this.barButtonItem7.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem7.ImageOptions.Image")));
            this.barButtonItem7.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem7.ImageOptions.LargeImage")));
            this.barButtonItem7.Name = "barButtonItem7";
            this.barButtonItem7.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem7_ItemClick);
            // 
            // گ
            // 
            this.گ.Caption = "گذارشات";
            this.گ.Id = 248;
            this.گ.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("گ.ImageOptions.Image")));
            this.گ.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("گ.ImageOptions.LargeImage")));
            this.گ.Name = "گ";
            this.گ.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.گ_ItemClick);
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "لیست شهر ها";
            this.barButtonItem8.Id = 249;
            this.barButtonItem8.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem8.ImageOptions.Image")));
            this.barButtonItem8.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem8.ImageOptions.LargeImage")));
            this.barButtonItem8.Name = "barButtonItem8";
            this.barButtonItem8.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem8_ItemClick);
            // 
            // menu_region
            // 
            this.menu_region.Caption = "لیست منطقه ها";
            this.menu_region.Id = 250;
            this.menu_region.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("menu_region.ImageOptions.Image")));
            this.menu_region.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("menu_region.ImageOptions.LargeImage")));
            this.menu_region.Name = "menu_region";
            this.menu_region.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.menu_region_ItemClick);
            // 
            // menu_distract
            // 
            this.menu_distract.Caption = "لیست ناحیه";
            this.menu_distract.Id = 251;
            this.menu_distract.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("menu_distract.ImageOptions.Image")));
            this.menu_distract.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("menu_distract.ImageOptions.LargeImage")));
            this.menu_distract.Name = "menu_distract";
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "لیست محدوده ها";
            this.barButtonItem9.Id = 252;
            this.barButtonItem9.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem9.ImageOptions.Image")));
            this.barButtonItem9.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem9.ImageOptions.LargeImage")));
            this.barButtonItem9.Name = "barButtonItem9";
            this.barButtonItem9.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem9_ItemClick);
            // 
            // barButtonItem10
            // 
            this.barButtonItem10.Caption = "لیست مسیر ها";
            this.barButtonItem10.Id = 253;
            this.barButtonItem10.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem10.ImageOptions.Image")));
            this.barButtonItem10.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem10.ImageOptions.LargeImage")));
            this.barButtonItem10.Name = "barButtonItem10";
            this.barButtonItem10.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem10_ItemClick);
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "barButtonItem6";
            this.barButtonItem6.Id = 254;
            this.barButtonItem6.Name = "barButtonItem6";
            this.barButtonItem6.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem6_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "صفحه نخست";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.BtnClose);
            this.ribbonPageGroup1.ItemLinks.Add(this.BarShowMap);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem4);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem6);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "امکانات";
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup2,
            this.ribbonPageGroup3});
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "تنظیمات";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItem2);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "امکانات";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "مدیریت کاربران";
            this.ribbonPageGroup3.Visible = false;
            // 
            // ribbonPage3
            // 
            this.ribbonPage3.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup4});
            this.ribbonPage3.Name = "ribbonPage3";
            this.ribbonPage3.Text = "نقشه";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.barToggleSwitchdrag);
            this.ribbonPageGroup4.ItemLinks.Add(this.barToggleSwitchscroll);
            this.ribbonPageGroup4.ItemLinks.Add(this.barToggleSwitchpolygon);
            this.ribbonPageGroup4.ItemLinks.Add(this.barEditItem1);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "امکانات نقشه";
            // 
            // ribbonPage4
            // 
            this.ribbonPage4.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup5,
            this.ribbonPageGroup7,
            this.ribbonPageGroup8,
            this.ribbonPageGroup9});
            this.ribbonPage4.Name = "ribbonPage4";
            this.ribbonPage4.Text = "مشتریان";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroup5.ItemLinks.Add(this.BarBtnImportForm);
            this.ribbonPageGroup5.ItemLinks.Add(this.barButtonItem3);
            this.ribbonPageGroup5.ItemLinks.Add(this.menu_region);
            this.ribbonPageGroup5.ItemLinks.Add(this.menu_distract);
            this.ribbonPageGroup5.ItemLinks.Add(this.barButtonItem9);
            this.ribbonPageGroup5.ItemLinks.Add(this.barButtonItem10);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.Text = "امکانات";
            // 
            // ribbonPageGroup7
            // 
            this.ribbonPageGroup7.ItemLinks.Add(this.barButtonItem7);
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            this.ribbonPageGroup7.Text = "تقسیم بندی";
            // 
            // ribbonPageGroup8
            // 
            this.ribbonPageGroup8.ItemLinks.Add(this.گ);
            this.ribbonPageGroup8.Name = "ribbonPageGroup8";
            this.ribbonPageGroup8.Text = "گزارشات";
            // 
            // ribbonPageGroup9
            // 
            this.ribbonPageGroup9.ItemLinks.Add(this.barButtonItem8);
            this.ribbonPageGroup9.Name = "ribbonPageGroup9";
            this.ribbonPageGroup9.Text = "ribbonPageGroup9";
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            -1,
            -1,
            -1,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit2.MaxValue = new decimal(new int[] {
            -1,
            -1,
            -1,
            0});
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            // 
            // repositoryItemSpinEdit3
            // 
            this.repositoryItemSpinEdit3.AutoHeight = false;
            this.repositoryItemSpinEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit3.MaxValue = new decimal(new int[] {
            -1,
            -1,
            -1,
            0});
            this.repositoryItemSpinEdit3.Name = "repositoryItemSpinEdit3";
            // 
            // repositoryItemSpinEdit4
            // 
            this.repositoryItemSpinEdit4.AutoHeight = false;
            this.repositoryItemSpinEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit4.MaxValue = new decimal(new int[] {
            -1,
            -1,
            -1,
            0});
            this.repositoryItemSpinEdit4.Name = "repositoryItemSpinEdit4";
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            // 
            // repositoryItemZoomTrackBar2
            // 
            this.repositoryItemZoomTrackBar2.Alignment = DevExpress.Utils.VertAlignment.Center;
            this.repositoryItemZoomTrackBar2.AllowFocused = false;
            this.repositoryItemZoomTrackBar2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.repositoryItemZoomTrackBar2.Maximum = 180;
            this.repositoryItemZoomTrackBar2.Name = "repositoryItemZoomTrackBar2";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.barStaticItem1);
            this.ribbonStatusBar.ItemLinks.Add(this.BtnMapDB);
            this.ribbonStatusBar.ItemLinks.Add(this.barStaticItem2);
            this.ribbonStatusBar.ItemLinks.Add(this.barStaticItem3);
            this.ribbonStatusBar.ItemLinks.Add(this.barStaticItem4);
            this.ribbonStatusBar.ItemLinks.Add(this.BarTxtSizeMap);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 708);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1228, 20);
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel5});
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel4,
            this.dockPanel3,
            this.dockPanel2});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane",
            "DevExpress.XtraBars.TabFormControl",
            "DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl",
            "DevExpress.XtraBars.ToolbarForm.ToolbarFormControl"});
            // 
            // dockPanel5
            // 
            this.dockPanel5.Controls.Add(this.dockPanel5_Container);
            this.dockPanel5.Dock = DevExpress.XtraBars.Docking.DockingStyle.Float;
            this.dockPanel5.FloatLocation = new System.Drawing.Point(975, 236);
            this.dockPanel5.ID = new System.Guid("62f298ae-1ef7-4625-bf42-e6acb121d4a8");
            this.dockPanel5.Location = new System.Drawing.Point(-32768, -32768);
            this.dockPanel5.Name = "dockPanel5";
            this.dockPanel5.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockPanel5.SavedIndex = 0;
            this.dockPanel5.Size = new System.Drawing.Size(200, 200);
            this.dockPanel5.Text = "لیست مشتریان";
            this.dockPanel5.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            // 
            // dockPanel5_Container
            // 
            this.dockPanel5_Container.Controls.Add(this.gridControl1);
            this.dockPanel5_Container.Location = new System.Drawing.Point(4, 21);
            this.dockPanel5_Container.Name = "dockPanel5_Container";
            this.dockPanel5_Container.Size = new System.Drawing.Size(192, 176);
            this.dockPanel5_Container.TabIndex = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = typeof(RoutingSystems.Model.Customer);
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.ribbon;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(192, 176);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // dockPanel4
            // 
            this.dockPanel4.Controls.Add(this.dockPanel4_Container);
            this.dockPanel4.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel4.ID = new System.Guid("6e026fed-8d13-4ad2-8cca-39c1e8e92a76");
            this.dockPanel4.Location = new System.Drawing.Point(0, 130);
            this.dockPanel4.Name = "dockPanel4";
            this.dockPanel4.OriginalSize = new System.Drawing.Size(234, 200);
            this.dockPanel4.Size = new System.Drawing.Size(234, 578);
            this.dockPanel4.Text = "لیست کلی";
            // 
            // dockPanel4_Container
            // 
            this.dockPanel4_Container.Controls.Add(this.tabPane1);
            this.dockPanel4_Container.Location = new System.Drawing.Point(4, 19);
            this.dockPanel4_Container.Name = "dockPanel4_Container";
            this.dockPanel4_Container.Size = new System.Drawing.Size(227, 556);
            this.dockPanel4_Container.TabIndex = 0;
            // 
            // tabPane1
            // 
            this.tabPane1.Controls.Add(this.tabNavigationPage1);
            this.tabPane1.Controls.Add(this.tabNavigationPage2);
            this.tabPane1.Controls.Add(this.tabNavigationPage3);
            this.tabPane1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPane1.Location = new System.Drawing.Point(0, 0);
            this.tabPane1.Name = "tabPane1";
            this.tabPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabNavigationPage1,
            this.tabNavigationPage2,
            this.tabNavigationPage3});
            this.tabPane1.RegularSize = new System.Drawing.Size(227, 556);
            this.tabPane1.SelectedPage = this.tabNavigationPage2;
            this.tabPane1.Size = new System.Drawing.Size(227, 556);
            this.tabPane1.TabIndex = 0;
            this.tabPane1.Text = "tabPane1";
            // 
            // tabNavigationPage1
            // 
            this.tabNavigationPage1.Caption = "فیلتر سفارشی";
            this.tabNavigationPage1.Controls.Add(this.simpleButton5);
            this.tabNavigationPage1.Controls.Add(this.checkEdit3);
            this.tabNavigationPage1.Controls.Add(this.checkEdit2);
            this.tabNavigationPage1.Controls.Add(this.checkEdit1);
            this.tabNavigationPage1.Controls.Add(this.simpleButton3);
            this.tabNavigationPage1.Controls.Add(this.listBox1);
            this.tabNavigationPage1.Controls.Add(this.labelControl58);
            this.tabNavigationPage1.Controls.Add(this.labelControl57);
            this.tabNavigationPage1.Controls.Add(this.labelControl56);
            this.tabNavigationPage1.Controls.Add(this.labelControl55);
            this.tabNavigationPage1.Controls.Add(this.labelControl54);
            this.tabNavigationPage1.Controls.Add(this.radioButton3);
            this.tabNavigationPage1.Controls.Add(this.radioButton2);
            this.tabNavigationPage1.Controls.Add(this.radioButton1);
            this.tabNavigationPage1.Controls.Add(this.simpleButton2);
            this.tabNavigationPage1.Controls.Add(this.simpleButton1);
            this.tabNavigationPage1.Controls.Add(this.comboBoxEdit5);
            this.tabNavigationPage1.Controls.Add(this.groupControl1);
            this.tabNavigationPage1.Controls.Add(this.comboBoxEdit4);
            this.tabNavigationPage1.Controls.Add(this.comboBoxEdit3);
            this.tabNavigationPage1.Controls.Add(this.comboBoxEdit2);
            this.tabNavigationPage1.Controls.Add(this.comboBoxEdit1);
            this.tabNavigationPage1.Name = "tabNavigationPage1";
            this.tabNavigationPage1.Size = new System.Drawing.Size(227, 540);
            this.tabNavigationPage1.Paint += new System.Windows.Forms.PaintEventHandler(this.tabNavigationPage1_Paint);
            // 
            // checkEdit3
            // 
            this.checkEdit3.Location = new System.Drawing.Point(4, 66);
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "";
            this.checkEdit3.Size = new System.Drawing.Size(21, 18);
            this.checkEdit3.TabIndex = 22;
            this.checkEdit3.CheckedChanged += new System.EventHandler(this.checkEdit3_CheckedChanged);
            // 
            // checkEdit2
            // 
            this.checkEdit2.Location = new System.Drawing.Point(4, 91);
            this.checkEdit2.MenuManager = this.ribbon;
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "";
            this.checkEdit2.Size = new System.Drawing.Size(21, 18);
            this.checkEdit2.TabIndex = 22;
            this.checkEdit2.CheckedChanged += new System.EventHandler(this.checkEdit2_CheckedChanged);
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(4, 118);
            this.checkEdit1.MenuManager = this.ribbon;
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "";
            this.checkEdit1.Size = new System.Drawing.Size(21, 18);
            this.checkEdit1.TabIndex = 21;
            this.checkEdit1.CheckedChanged += new System.EventHandler(this.checkEdit1_CheckedChanged);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(7, 282);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(217, 95);
            this.listBox1.TabIndex = 19;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // labelControl58
            // 
            this.labelControl58.Location = new System.Drawing.Point(192, 119);
            this.labelControl58.Name = "labelControl58";
            this.labelControl58.Size = new System.Drawing.Size(27, 13);
            this.labelControl58.TabIndex = 18;
            this.labelControl58.Text = "مسیر";
            // 
            // labelControl57
            // 
            this.labelControl57.Location = new System.Drawing.Point(185, 93);
            this.labelControl57.Name = "labelControl57";
            this.labelControl57.Size = new System.Drawing.Size(34, 13);
            this.labelControl57.TabIndex = 17;
            this.labelControl57.Text = "محدوده";
            // 
            // labelControl56
            // 
            this.labelControl56.Location = new System.Drawing.Point(196, 67);
            this.labelControl56.Name = "labelControl56";
            this.labelControl56.Size = new System.Drawing.Size(23, 13);
            this.labelControl56.TabIndex = 16;
            this.labelControl56.Text = "ناحیه";
            // 
            // labelControl55
            // 
            this.labelControl55.Location = new System.Drawing.Point(189, 41);
            this.labelControl55.Name = "labelControl55";
            this.labelControl55.Size = new System.Drawing.Size(30, 13);
            this.labelControl55.TabIndex = 15;
            this.labelControl55.Text = "منطقه";
            // 
            // labelControl54
            // 
            this.labelControl54.Location = new System.Drawing.Point(197, 15);
            this.labelControl54.Name = "labelControl54";
            this.labelControl54.Size = new System.Drawing.Size(22, 13);
            this.labelControl54.TabIndex = 14;
            this.labelControl54.Text = "شهر";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Checked = true;
            this.radioButton3.Location = new System.Drawing.Point(140, 142);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(79, 17);
            this.radioButton3.TabIndex = 13;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "نمایش همه";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(78, 188);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(141, 17);
            this.radioButton2.TabIndex = 12;
            this.radioButton2.Text = "نمایش مشتریان غیر فعال";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(97, 165);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(122, 17);
            this.radioButton1.TabIndex = 11;
            this.radioButton1.Text = "نمایش مشتریان فعال";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(3, 508);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(222, 23);
            this.simpleButton2.TabIndex = 10;
            this.simpleButton2.Text = "بازنوبسی فرم";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(3, 479);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(222, 23);
            this.simpleButton1.TabIndex = 9;
            this.simpleButton1.Text = "اجرای فیلتر";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // comboBoxEdit5
            // 
            this.comboBoxEdit5.Location = new System.Drawing.Point(31, 116);
            this.comboBoxEdit5.MenuManager = this.ribbon;
            this.comboBoxEdit5.Name = "comboBoxEdit5";
            this.comboBoxEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit5.Size = new System.Drawing.Size(112, 20);
            this.comboBoxEdit5.TabIndex = 8;
            this.comboBoxEdit5.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit5_SelectedIndexChanged);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.labelControl53);
            this.groupControl1.Controls.Add(this.countfilter);
            this.groupControl1.GroupStyle = DevExpress.Utils.GroupStyle.Light;
            this.groupControl1.Location = new System.Drawing.Point(2, 211);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(222, 65);
            this.groupControl1.TabIndex = 5;
            this.groupControl1.Text = "آمار";
            this.groupControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.groupControl1_Paint);
            // 
            // labelControl53
            // 
            this.labelControl53.Location = new System.Drawing.Point(148, 23);
            this.labelControl53.Name = "labelControl53";
            this.labelControl53.Size = new System.Drawing.Size(63, 13);
            this.labelControl53.TabIndex = 37;
            this.labelControl53.Text = "تعداد مشتری";
            // 
            // countfilter
            // 
            this.countfilter.Location = new System.Drawing.Point(5, 23);
            this.countfilter.Name = "countfilter";
            this.countfilter.Size = new System.Drawing.Size(6, 13);
            this.countfilter.TabIndex = 36;
            this.countfilter.Text = "0";
            // 
            // comboBoxEdit4
            // 
            this.comboBoxEdit4.Location = new System.Drawing.Point(31, 90);
            this.comboBoxEdit4.MenuManager = this.ribbon;
            this.comboBoxEdit4.Name = "comboBoxEdit4";
            this.comboBoxEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit4.Size = new System.Drawing.Size(112, 20);
            this.comboBoxEdit4.TabIndex = 4;
            this.comboBoxEdit4.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit4_SelectedIndexChanged);
            // 
            // comboBoxEdit3
            // 
            this.comboBoxEdit3.Location = new System.Drawing.Point(30, 64);
            this.comboBoxEdit3.MenuManager = this.ribbon;
            this.comboBoxEdit3.Name = "comboBoxEdit3";
            this.comboBoxEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit3.Size = new System.Drawing.Size(112, 20);
            this.comboBoxEdit3.TabIndex = 3;
            this.comboBoxEdit3.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit3_SelectedIndexChanged);
            // 
            // comboBoxEdit2
            // 
            this.comboBoxEdit2.Location = new System.Drawing.Point(31, 38);
            this.comboBoxEdit2.MenuManager = this.ribbon;
            this.comboBoxEdit2.Name = "comboBoxEdit2";
            this.comboBoxEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit2.Size = new System.Drawing.Size(112, 20);
            this.comboBoxEdit2.TabIndex = 2;
            this.comboBoxEdit2.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit2_SelectedIndexChanged);
            // 
            // comboBoxEdit1
            // 
            this.comboBoxEdit1.Location = new System.Drawing.Point(31, 12);
            this.comboBoxEdit1.MenuManager = this.ribbon;
            this.comboBoxEdit1.Name = "comboBoxEdit1";
            this.comboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit1.Size = new System.Drawing.Size(112, 20);
            this.comboBoxEdit1.TabIndex = 0;
            this.comboBoxEdit1.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit1_SelectedIndexChanged);
            // 
            // tabNavigationPage2
            // 
            this.tabNavigationPage2.Caption = "فیلتر درختی";
            this.tabNavigationPage2.Controls.Add(this.treeView1);
            this.tabNavigationPage2.Name = "tabNavigationPage2";
            this.tabNavigationPage2.Size = new System.Drawing.Size(227, 540);
            // 
            // treeView1
            // 
            this.treeView1.CheckBoxes = true;
            this.treeView1.ContextMenuStrip = this.contextMenuStrip1;
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.Location = new System.Drawing.Point(0, 0);
            this.treeView1.Name = "treeView1";
            this.treeView1.RightToLeftLayout = true;
            this.treeView1.Size = new System.Drawing.Size(227, 540);
            this.treeView1.TabIndex = 1;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.نمایشمسیرToolStripMenuItem,
            this.تستToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.contextMenuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.contextMenuStrip1.ShowCheckMargin = true;
            this.contextMenuStrip1.Size = new System.Drawing.Size(158, 48);
            // 
            // نمایشمسیرToolStripMenuItem
            // 
            this.نمایشمسیرToolStripMenuItem.Name = "نمایشمسیرToolStripMenuItem";
            this.نمایشمسیرToolStripMenuItem.RightToLeftAutoMirrorImage = true;
            this.نمایشمسیرToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.نمایشمسیرToolStripMenuItem.Text = "نمایش مسیر";
            this.نمایشمسیرToolStripMenuItem.Click += new System.EventHandler(this.نمایشمسیرToolStripMenuItem_Click);
            // 
            // تستToolStripMenuItem
            // 
            this.تستToolStripMenuItem.Name = "تستToolStripMenuItem";
            this.تستToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.تستToolStripMenuItem.Text = "تست";
            this.تستToolStripMenuItem.Click += new System.EventHandler(this.تستToolStripMenuItem_Click);
            // 
            // tabNavigationPage3
            // 
            this.tabNavigationPage3.Caption = "جزییات";
            this.tabNavigationPage3.Controls.Add(this.simpleButton4);
            this.tabNavigationPage3.Controls.Add(this.listBox2);
            this.tabNavigationPage3.Name = "tabNavigationPage3";
            this.tabNavigationPage3.Size = new System.Drawing.Size(227, 540);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(3, 507);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(222, 23);
            this.simpleButton4.TabIndex = 10;
            this.simpleButton4.Text = "خالی کردن";
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(3, 3);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(222, 498);
            this.listBox2.TabIndex = 0;
            // 
            // dockPanel3
            // 
            this.dockPanel3.Controls.Add(this.dockPanel3_Container);
            this.dockPanel3.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanel3.ID = new System.Guid("aa1e41c3-2b38-4e1e-9bdf-2fa07d55e191");
            this.dockPanel3.Location = new System.Drawing.Point(970, 130);
            this.dockPanel3.Name = "dockPanel3";
            this.dockPanel3.OriginalSize = new System.Drawing.Size(258, 200);
            this.dockPanel3.Size = new System.Drawing.Size(258, 578);
            this.dockPanel3.Text = "اطلاعات مشتری";
            // 
            // dockPanel3_Container
            // 
            this.dockPanel3_Container.Controls.Add(this.update_at);
            this.dockPanel3_Container.Controls.Add(this.way);
            this.dockPanel3_Container.Controls.Add(this.sell_center_name);
            this.dockPanel3_Container.Controls.Add(this.type_customer);
            this.dockPanel3_Container.Controls.Add(this.create_at);
            this.dockPanel3_Container.Controls.Add(this.create_at_Faaliat);
            this.dockPanel3_Container.Controls.Add(this.city);
            this.dockPanel3_Container.Controls.Add(this.region);
            this.dockPanel3_Container.Controls.Add(this.distract);
            this.dockPanel3_Container.Controls.Add(this.rang);
            this.dockPanel3_Container.Controls.Add(this.postal_code);
            this.dockPanel3_Container.Controls.Add(this.economic_code);
            this.dockPanel3_Container.Controls.Add(this.address);
            this.dockPanel3_Container.Controls.Add(this.market);
            this.dockPanel3_Container.Controls.Add(this.activity);
            this.dockPanel3_Container.Controls.Add(this.labelControl36);
            this.dockPanel3_Container.Controls.Add(this.labelControl35);
            this.dockPanel3_Container.Controls.Add(this.labelControl34);
            this.dockPanel3_Container.Controls.Add(this.labelControl32);
            this.dockPanel3_Container.Controls.Add(this.labelControl31);
            this.dockPanel3_Container.Controls.Add(this.labelControl30);
            this.dockPanel3_Container.Controls.Add(this.labelControl29);
            this.dockPanel3_Container.Controls.Add(this.labelControl28);
            this.dockPanel3_Container.Controls.Add(this.labelControl27);
            this.dockPanel3_Container.Controls.Add(this.labelControl26);
            this.dockPanel3_Container.Controls.Add(this.labelControl25);
            this.dockPanel3_Container.Controls.Add(this.labelControl24);
            this.dockPanel3_Container.Controls.Add(this.labelControl23);
            this.dockPanel3_Container.Controls.Add(this.labelControl22);
            this.dockPanel3_Container.Controls.Add(this.labelControl21);
            this.dockPanel3_Container.Controls.Add(this.national_code);
            this.dockPanel3_Container.Controls.Add(this.phone_number_2);
            this.dockPanel3_Container.Controls.Add(this.phone_number);
            this.dockPanel3_Container.Controls.Add(this.home_number_2);
            this.dockPanel3_Container.Controls.Add(this.home_number);
            this.dockPanel3_Container.Controls.Add(this.Saller_id);
            this.dockPanel3_Container.Controls.Add(this.saller_name);
            this.dockPanel3_Container.Controls.Add(this.bisiness);
            this.dockPanel3_Container.Controls.Add(this.name);
            this.dockPanel3_Container.Controls.Add(this.customer_id);
            this.dockPanel3_Container.Controls.Add(this.labelControl10);
            this.dockPanel3_Container.Controls.Add(this.labelControl9);
            this.dockPanel3_Container.Controls.Add(this.labelControl8);
            this.dockPanel3_Container.Controls.Add(this.labelControl7);
            this.dockPanel3_Container.Controls.Add(this.labelControl6);
            this.dockPanel3_Container.Controls.Add(this.labelControl5);
            this.dockPanel3_Container.Controls.Add(this.labelControl4);
            this.dockPanel3_Container.Controls.Add(this.labelControl3);
            this.dockPanel3_Container.Controls.Add(this.labelControl2);
            this.dockPanel3_Container.Controls.Add(this.labelControl1);
            this.dockPanel3_Container.Location = new System.Drawing.Point(3, 19);
            this.dockPanel3_Container.Name = "dockPanel3_Container";
            this.dockPanel3_Container.Size = new System.Drawing.Size(251, 556);
            this.dockPanel3_Container.TabIndex = 0;
            // 
            // update_at
            // 
            this.update_at.Location = new System.Drawing.Point(3, 511);
            this.update_at.Name = "update_at";
            this.update_at.Size = new System.Drawing.Size(3, 13);
            this.update_at.TabIndex = 50;
            this.update_at.Text = " ";
            this.update_at.Visible = false;
            // 
            // way
            // 
            this.way.Location = new System.Drawing.Point(3, 416);
            this.way.Name = "way";
            this.way.Size = new System.Drawing.Size(3, 13);
            this.way.TabIndex = 49;
            this.way.Text = " ";
            // 
            // sell_center_name
            // 
            this.sell_center_name.Location = new System.Drawing.Point(3, 435);
            this.sell_center_name.Name = "sell_center_name";
            this.sell_center_name.Size = new System.Drawing.Size(3, 13);
            this.sell_center_name.TabIndex = 48;
            this.sell_center_name.Text = " ";
            // 
            // type_customer
            // 
            this.type_customer.Location = new System.Drawing.Point(3, 454);
            this.type_customer.Name = "type_customer";
            this.type_customer.Size = new System.Drawing.Size(3, 13);
            this.type_customer.TabIndex = 47;
            this.type_customer.Text = " ";
            // 
            // create_at
            // 
            this.create_at.Location = new System.Drawing.Point(3, 492);
            this.create_at.Name = "create_at";
            this.create_at.Size = new System.Drawing.Size(3, 13);
            this.create_at.TabIndex = 45;
            this.create_at.Text = " ";
            this.create_at.Visible = false;
            // 
            // create_at_Faaliat
            // 
            this.create_at_Faaliat.Location = new System.Drawing.Point(3, 321);
            this.create_at_Faaliat.Name = "create_at_Faaliat";
            this.create_at_Faaliat.Size = new System.Drawing.Size(3, 13);
            this.create_at_Faaliat.TabIndex = 44;
            this.create_at_Faaliat.Text = " ";
            // 
            // city
            // 
            this.city.Location = new System.Drawing.Point(3, 340);
            this.city.Name = "city";
            this.city.Size = new System.Drawing.Size(3, 13);
            this.city.TabIndex = 43;
            this.city.Text = " ";
            // 
            // region
            // 
            this.region.Location = new System.Drawing.Point(3, 359);
            this.region.Name = "region";
            this.region.Size = new System.Drawing.Size(3, 13);
            this.region.TabIndex = 42;
            this.region.Text = " ";
            // 
            // distract
            // 
            this.distract.Location = new System.Drawing.Point(3, 378);
            this.distract.Name = "distract";
            this.distract.Size = new System.Drawing.Size(3, 13);
            this.distract.TabIndex = 41;
            this.distract.Text = " ";
            // 
            // rang
            // 
            this.rang.Location = new System.Drawing.Point(3, 397);
            this.rang.Name = "rang";
            this.rang.Size = new System.Drawing.Size(3, 13);
            this.rang.TabIndex = 40;
            this.rang.Text = " ";
            // 
            // postal_code
            // 
            this.postal_code.Location = new System.Drawing.Point(3, 193);
            this.postal_code.Name = "postal_code";
            this.postal_code.Size = new System.Drawing.Size(3, 13);
            this.postal_code.TabIndex = 39;
            this.postal_code.Text = " ";
            // 
            // economic_code
            // 
            this.economic_code.Location = new System.Drawing.Point(3, 212);
            this.economic_code.Name = "economic_code";
            this.economic_code.Size = new System.Drawing.Size(0, 13);
            this.economic_code.TabIndex = 38;
            // 
            // address
            // 
            this.address.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.address.LineLocation = DevExpress.XtraEditors.LineLocation.Right;
            this.address.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal;
            this.address.LineVisible = true;
            this.address.Location = new System.Drawing.Point(43, 250);
            this.address.Name = "address";
            this.address.Size = new System.Drawing.Size(6, 13);
            this.address.TabIndex = 37;
            this.address.Text = " ";
            this.address.Click += new System.EventHandler(this.address_Click);
            // 
            // market
            // 
            this.market.Location = new System.Drawing.Point(3, 302);
            this.market.Name = "market";
            this.market.Size = new System.Drawing.Size(3, 13);
            this.market.TabIndex = 36;
            this.market.Text = " ";
            // 
            // activity
            // 
            this.activity.Location = new System.Drawing.Point(3, 283);
            this.activity.Name = "activity";
            this.activity.Size = new System.Drawing.Size(3, 13);
            this.activity.TabIndex = 35;
            this.activity.Text = " ";
            // 
            // labelControl36
            // 
            this.labelControl36.Location = new System.Drawing.Point(156, 511);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(93, 13);
            this.labelControl36.TabIndex = 34;
            this.labelControl36.Text = "تاریخ آخرین ویرایش :";
            this.labelControl36.Visible = false;
            // 
            // labelControl35
            // 
            this.labelControl35.Location = new System.Drawing.Point(196, 492);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(52, 13);
            this.labelControl35.TabIndex = 33;
            this.labelControl35.Text = "تاریخ ایجاد :";
            this.labelControl35.Visible = false;
            // 
            // labelControl34
            // 
            this.labelControl34.Location = new System.Drawing.Point(186, 454);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(62, 13);
            this.labelControl34.TabIndex = 32;
            this.labelControl34.Text = "نوع مشتری :";
            // 
            // labelControl32
            // 
            this.labelControl32.Location = new System.Drawing.Point(218, 378);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(30, 13);
            this.labelControl32.TabIndex = 30;
            this.labelControl32.Text = "ناحیه :";
            // 
            // labelControl31
            // 
            this.labelControl31.Location = new System.Drawing.Point(207, 397);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(41, 13);
            this.labelControl31.TabIndex = 29;
            this.labelControl31.Text = "محدوده :";
            // 
            // labelControl30
            // 
            this.labelControl30.Location = new System.Drawing.Point(214, 416);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(34, 13);
            this.labelControl30.TabIndex = 28;
            this.labelControl30.Text = "مسیر :";
            // 
            // labelControl29
            // 
            this.labelControl29.Location = new System.Drawing.Point(189, 435);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(59, 13);
            this.labelControl29.TabIndex = 27;
            this.labelControl29.Text = "مرکز فروش :";
            // 
            // labelControl28
            // 
            this.labelControl28.Location = new System.Drawing.Point(195, 302);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(53, 13);
            this.labelControl28.TabIndex = 26;
            this.labelControl28.Text = "نوع مارکت :";
            // 
            // labelControl27
            // 
            this.labelControl27.Location = new System.Drawing.Point(152, 321);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(96, 13);
            this.labelControl27.TabIndex = 25;
            this.labelControl27.Text = "تاریخ شروع فعالیت : ";
            // 
            // labelControl26
            // 
            this.labelControl26.Location = new System.Drawing.Point(219, 340);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(29, 13);
            this.labelControl26.TabIndex = 24;
            this.labelControl26.Text = "شهر :";
            // 
            // labelControl25
            // 
            this.labelControl25.Location = new System.Drawing.Point(211, 359);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(37, 13);
            this.labelControl25.TabIndex = 23;
            this.labelControl25.Text = "منطقه :";
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(197, 193);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(51, 13);
            this.labelControl24.TabIndex = 22;
            this.labelControl24.Text = "کد پستی :";
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(156, 155);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(92, 13);
            this.labelControl23.TabIndex = 21;
            this.labelControl23.Text = "شماره تلفن ثابت 2 :";
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(151, 117);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(97, 13);
            this.labelControl22.TabIndex = 20;
            this.labelControl22.Text = "شماره تماس ثابت 2 :";
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(190, 3);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(58, 13);
            this.labelControl21.TabIndex = 19;
            this.labelControl21.Text = "کد مشتری :";
            // 
            // national_code
            // 
            this.national_code.Location = new System.Drawing.Point(3, 174);
            this.national_code.Name = "national_code";
            this.national_code.Size = new System.Drawing.Size(3, 13);
            this.national_code.TabIndex = 18;
            this.national_code.Text = " ";
            // 
            // phone_number_2
            // 
            this.phone_number_2.Location = new System.Drawing.Point(3, 155);
            this.phone_number_2.Name = "phone_number_2";
            this.phone_number_2.Size = new System.Drawing.Size(3, 13);
            this.phone_number_2.TabIndex = 17;
            this.phone_number_2.Text = " ";
            // 
            // phone_number
            // 
            this.phone_number.Location = new System.Drawing.Point(3, 136);
            this.phone_number.Name = "phone_number";
            this.phone_number.Size = new System.Drawing.Size(3, 13);
            this.phone_number.TabIndex = 16;
            this.phone_number.Text = " ";
            // 
            // home_number_2
            // 
            this.home_number_2.Location = new System.Drawing.Point(3, 117);
            this.home_number_2.Name = "home_number_2";
            this.home_number_2.Size = new System.Drawing.Size(3, 13);
            this.home_number_2.TabIndex = 15;
            this.home_number_2.Text = " ";
            // 
            // home_number
            // 
            this.home_number.Location = new System.Drawing.Point(3, 98);
            this.home_number.Name = "home_number";
            this.home_number.Size = new System.Drawing.Size(3, 13);
            this.home_number.TabIndex = 14;
            this.home_number.Text = " ";
            // 
            // Saller_id
            // 
            this.Saller_id.Location = new System.Drawing.Point(3, 79);
            this.Saller_id.Name = "Saller_id";
            this.Saller_id.Size = new System.Drawing.Size(3, 13);
            this.Saller_id.TabIndex = 13;
            this.Saller_id.Text = " ";
            // 
            // saller_name
            // 
            this.saller_name.Location = new System.Drawing.Point(3, 60);
            this.saller_name.Name = "saller_name";
            this.saller_name.Size = new System.Drawing.Size(3, 13);
            this.saller_name.TabIndex = 12;
            this.saller_name.Text = " ";
            // 
            // bisiness
            // 
            this.bisiness.Location = new System.Drawing.Point(3, 41);
            this.bisiness.Name = "bisiness";
            this.bisiness.Size = new System.Drawing.Size(3, 13);
            this.bisiness.TabIndex = 11;
            this.bisiness.Text = " ";
            // 
            // name
            // 
            this.name.Location = new System.Drawing.Point(3, 22);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(3, 13);
            this.name.TabIndex = 10;
            this.name.Text = " ";
            this.name.Click += new System.EventHandler(this.name_Click);
            // 
            // customer_id
            // 
            this.customer_id.Location = new System.Drawing.Point(3, 3);
            this.customer_id.Name = "customer_id";
            this.customer_id.Size = new System.Drawing.Size(3, 13);
            this.customer_id.TabIndex = 9;
            this.customer_id.Text = " ";
            this.customer_id.Click += new System.EventHandler(this.customer_id_Click);
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(216, 231);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(32, 13);
            this.labelControl10.TabIndex = 5;
            this.labelControl10.Text = "آدرس :";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(188, 212);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(60, 13);
            this.labelControl9.TabIndex = 8;
            this.labelControl9.Text = "کد اقتصادی :";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(209, 174);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(39, 13);
            this.labelControl8.TabIndex = 7;
            this.labelControl8.Text = "کدملی :";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(165, 136);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(83, 13);
            this.labelControl7.TabIndex = 6;
            this.labelControl7.Text = "شماره تلفن ثابت :";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(160, 98);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(88, 13);
            this.labelControl6.TabIndex = 5;
            this.labelControl6.Text = "شماره تماس ثابت :";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(186, 79);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(62, 13);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "کد فروشنده :";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(184, 60);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(64, 13);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "نام فروشنده :";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(175, 41);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(73, 13);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "نام کسب و کار :";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(192, 283);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(56, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "نوع فعالیت :";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(159, 22);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(89, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "نام و نام خانوادگی :";
            // 
            // dockPanel2
            // 
            this.dockPanel2.Controls.Add(this.dockPanel2_Container);
            this.dockPanel2.DockedAsTabbedDocument = true;
            this.dockPanel2.FloatLocation = new System.Drawing.Point(646, 146);
            this.dockPanel2.FloatSize = new System.Drawing.Size(852, 358);
            this.dockPanel2.ID = new System.Guid("b4265e54-e115-4bcf-aa9e-5d19765b20f6");
            this.dockPanel2.Name = "dockPanel2";
            this.dockPanel2.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockPanel2.SavedIndex = 3;
            this.dockPanel2.SavedMdiDocument = true;
            this.dockPanel2.Text = "نقشه";
            this.dockPanel2.Click += new System.EventHandler(this.dockPanel2_Click);
            // 
            // dockPanel2_Container
            // 
            this.dockPanel2_Container.Location = new System.Drawing.Point(0, 0);
            this.dockPanel2_Container.Name = "dockPanel2_Container";
            this.dockPanel2_Container.Size = new System.Drawing.Size(730, 555);
            this.dockPanel2_Container.TabIndex = 0;
            // 
            // documentManager1
            // 
            this.documentManager1.ContainerControl = this;
            this.documentManager1.View = this.tabbedView1;
            this.documentManager1.ViewCollection.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseView[] {
            this.tabbedView1});
            // 
            // tabbedView1
            // 
            this.tabbedView1.DocumentGroups.AddRange(new DevExpress.XtraBars.Docking2010.Views.Tabbed.DocumentGroup[] {
            this.documentGroup1});
            this.tabbedView1.Documents.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseDocument[] {
            this.document2});
            dockingContainer5.Element = this.documentGroup1;
            this.tabbedView1.RootContainer.Nodes.AddRange(new DevExpress.XtraBars.Docking2010.Views.Tabbed.DockingContainer[] {
            dockingContainer5});
            // 
            // splashScreenManager
            // 
            this.splashScreenManager.ClosingDelay = 500;
            // 
            // popupMenu1
            // 
            this.popupMenu1.ItemLinks.Add(this.barButtonItem5);
            this.popupMenu1.Name = "popupMenu1";
            this.popupMenu1.Ribbon = this.ribbon;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(3, 450);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(222, 23);
            this.simpleButton3.TabIndex = 20;
            this.simpleButton3.Text = "ثبت موقعیت شروع";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton5
            // 
            this.simpleButton5.Location = new System.Drawing.Point(3, 421);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(222, 23);
            this.simpleButton5.TabIndex = 23;
            this.simpleButton5.Text = "نمایش مسیر";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1228, 728);
            this.Controls.Add(this.dockPanel3);
            this.Controls.Add(this.dockPanel4);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.Name = "Main";
            this.Ribbon = this.ribbon;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "نرم افزار مسیر بندی";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.documentGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.document2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemZoomTrackBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel5.ResumeLayout(false);
            this.dockPanel5_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.dockPanel4.ResumeLayout(false);
            this.dockPanel4_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).EndInit();
            this.tabPane1.ResumeLayout(false);
            this.tabNavigationPage1.ResumeLayout(false);
            this.tabNavigationPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).EndInit();
            this.tabNavigationPage2.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabNavigationPage3.ResumeLayout(false);
            this.dockPanel3.ResumeLayout(false);
            this.dockPanel3_Container.ResumeLayout(false);
            this.dockPanel3_Container.PerformLayout();
            this.dockPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.documentManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.BarButtonItem BtnClose;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel2;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel2_Container;
        private DevExpress.XtraBars.Docking2010.DocumentManager documentManager1;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView tabbedView1;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.DocumentGroup documentGroup1;
        private DevExpress.XtraBars.BarButtonItem BarShowMap;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel3;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel3_Container;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel4;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel4_Container;
        private DevExpress.XtraBars.BarButtonItem BtnMapDB;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem4;
        private DevExpress.XtraBars.BarStaticItem BarTxtSizeMap;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel5;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel5_Container;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraBars.BarToggleSwitchItem barToggleSwitchdrag;
        private DevExpress.XtraBars.BarToggleSwitchItem barToggleSwitchscroll;
        private DevExpress.XtraBars.BarToggleSwitchItem barToggleSwitchpolygon;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar repositoryItemZoomTrackBar1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem BarBtnImportForm;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage4;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        public DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.Ribbon.ApplicationMenu applicationMenu1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
        private DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar repositoryItemZoomTrackBar2;
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.Document document2;
        private DevExpress.XtraEditors.LabelControl national_code;
        private DevExpress.XtraEditors.LabelControl phone_number_2;
        private DevExpress.XtraEditors.LabelControl phone_number;
        private DevExpress.XtraEditors.LabelControl home_number_2;
        private DevExpress.XtraEditors.LabelControl home_number;
        private DevExpress.XtraEditors.LabelControl Saller_id;
        private DevExpress.XtraEditors.LabelControl saller_name;
        private DevExpress.XtraEditors.LabelControl bisiness;
        private DevExpress.XtraEditors.LabelControl name;
        private DevExpress.XtraEditors.LabelControl customer_id;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.LabelControl update_at;
        private DevExpress.XtraEditors.LabelControl way;
        private DevExpress.XtraEditors.LabelControl sell_center_name;
        private DevExpress.XtraEditors.LabelControl type_customer;
        private DevExpress.XtraEditors.LabelControl create_at;
        private DevExpress.XtraEditors.LabelControl create_at_Faaliat;
        private DevExpress.XtraEditors.LabelControl city;
        private DevExpress.XtraEditors.LabelControl region;
        private DevExpress.XtraEditors.LabelControl distract;
        private DevExpress.XtraEditors.LabelControl rang;
        private DevExpress.XtraEditors.LabelControl postal_code;
        private DevExpress.XtraEditors.LabelControl economic_code;
        private DevExpress.XtraEditors.LabelControl address;
        private DevExpress.XtraEditors.LabelControl market;
        private DevExpress.XtraEditors.LabelControl activity;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem نمایشمسیرToolStripMenuItem;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.Navigation.TabPane tabPane1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit4;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit3;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit2;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage2;
        private System.Windows.Forms.TreeView treeView1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit5;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit1;
        private DevExpress.XtraEditors.LabelControl countfilter;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private DevExpress.XtraEditors.LabelControl labelControl53;
        private System.Windows.Forms.RadioButton radioButton3;
        private DevExpress.XtraEditors.LabelControl labelControl58;
        private DevExpress.XtraEditors.LabelControl labelControl57;
        private DevExpress.XtraEditors.LabelControl labelControl56;
        private DevExpress.XtraEditors.LabelControl labelControl55;
        private DevExpress.XtraEditors.LabelControl labelControl54;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.BarButtonItem گ;
        private System.Windows.Forms.ListBox listBox1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage3;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.ToolStripMenuItem تستToolStripMenuItem;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup9;
        private DevExpress.XtraBars.BarButtonItem menu_region;
        private DevExpress.XtraBars.BarButtonItem menu_distract;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private DevExpress.XtraBars.BarButtonItem barButtonItem10;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
    }
}