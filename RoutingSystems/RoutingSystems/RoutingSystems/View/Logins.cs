﻿using System;
using DevExpress.XtraSplashScreen;
using RoutingSystems.Controller;

namespace RoutingSystems.View
{
    public partial class Logins : DevExpress.XtraEditors.XtraForm
    {
        AdminController Controller;
        public Logins()
        {
            InitializeComponent();
            Controller = new AdminController();
        }
        private void BtnLogin_Click(object sender, EventArgs e)
        {
            LoadingController.ShowLoading(splashScreenManager1);
            int responce = Controller.Login(TxtBoxUsername.Text, TxtBoxPassword.Text);
            if(responce != 0)
            {
                LoadingController.HideLoading(splashScreenManager1);
                Main main = new Main();
                main.Show();
                this.Hide();
            }
            else
            {
                LoadingController.HideLoading(splashScreenManager1);
            }
        }
        private void BtnClose_Click(object sender, EventArgs e)
        {
            LoadingController.HideLoading(splashScreenManager1);
            Environment.Exit(1);
            LoadingController.HideLoading(splashScreenManager1);
        }
    }
}