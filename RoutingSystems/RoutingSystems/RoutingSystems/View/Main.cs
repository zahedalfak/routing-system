﻿using DevExpress.XtraBars;
using DevExpress.XtraSplashScreen;
using GMap.NET;
using GMap.NET.MapProviders;
using RoutingSystems.Controller;
using System;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using System.Data.Entity;
using RoutingSystems.Model;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using CefSharp;
using CefSharp.WinForms;
namespace RoutingSystems.View
{
    public partial class Main : DevExpress.XtraBars.Ribbon.RibbonForm, CallBackMainForAddRoutes
    {
        public static ChromiumWebBrowser chromeBrowser;
        MapMainController MapMainController;
        public Main()
        {
            InitializeComponent();
            InitializeChromium();
            MapMainController = new MapMainController();
          
            //gMap.Manager.OnTileCacheComplete += new TileCacheComplete(OnTileCacheComplete);
            //dockPanel4.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            //dockPanel2.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            //dockPanel3.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            //dockPanel5.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
        }
        private void BtnClose_ItemClick(object sender, ItemClickEventArgs e)
        {
            Environment.Exit(1);
        }
        private void BarShowMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            LoadingController.ShowLoading(splashScreenManager);
          
          
            //dockPanel4.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide;
            //dockPanel2.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
            //dockPanel3.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide;
            //test();
            LoadingController.HideLoading(splashScreenManager);
        }
        private void Main_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            LoadingController.ShowLoading(splashScreenManager);
            Environment.Exit(1);
            LoadingController.HideLoading(splashScreenManager);
        }
        private void barToggleSwitchdrag_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            LoadingController.ShowLoading(splashScreenManager);
           
            LoadingController.HideLoading(splashScreenManager);
        }
        private void barToggleSwitchscroll_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            LoadingController.ShowLoading(splashScreenManager);
           
            LoadingController.HideLoading(splashScreenManager);
        }
        private void barToggleSwitchpolygon_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            LoadingController.ShowLoading(splashScreenManager);
           
            LoadingController.HideLoading(splashScreenManager);
        }
        private void BarBtnImportForm_ItemClick(object sender, ItemClickEventArgs e)
        {
            LoadingController.ShowLoading(splashScreenManager);
            ExcelImport excelimportform = new ExcelImport();
            LoadingController.HideLoading(splashScreenManager);
            excelimportform.ShowDialog();
            
        }
        private void BtnMapDB_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                //string argument = "/select, \"" + gMap.CacheLocation + "TileDBv5\"";
                //System.Diagnostics.Process.Start("explorer.exe", argument);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to open: " + ex.Message, "نقشه", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void OnTileCacheComplete()
        {
            Debug.WriteLine("OnTileCacheComplete");
            long size = 0;
            int db = 0;
            try
            {
                /*
                DirectoryInfo di = new DirectoryInfo(gMap.CacheLocation);
                var dbs = di.GetFiles("*.gmdb", SearchOption.AllDirectories);
                foreach (var d in dbs)
                {
                    size += d.Length;
                    db++;
                }
                */
            }
            catch
            {
            }

            if (!IsDisposed)
            {
                MethodInvoker m = delegate
                {
                    BarTxtSizeMap.Caption = string.Format(CultureInfo.InvariantCulture, "{0} db in {1:00} MB", db, size / (1024.0 * 1024.0));
                };

                if (!IsDisposed)
                {
                    try
                    {
                        Invoke(m);
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }
        #region Filters
        Array item_city_id;
        Array item_Region_id;
        Array item_Distract_id;
        Array item_Range_id;
        Array item_Way_id;
        //
        int city_id = 0;
        String Regionid = "";
        String Distractid = "";
        String Rangid = "";
        String Wayid = "";
        int Status = 100;
        //
        rasharoutesystemEntities1 db = new rasharoutesystemEntities1();
        private void Main_Load(object sender, EventArgs e)
        {
            item_city_id = db.View_1.Select(x => x.id).ToArray();
            comboBoxEdit1.Text = "...موردی را انتخاب کنید";
            comboBoxEdit1.Properties.Items.Add("...موردی را انتخاب کنید");
            comboBoxEdit1.Properties.Items.AddRange(db.View_1.Select(x => x.name).ToArray());
            //
            comboBoxEdit2.Properties.Items.Clear();
            comboBoxEdit2.Text = "موردی موچود نیست";
            comboBoxEdit3.Properties.Items.Clear();
            comboBoxEdit3.Text = "موردی موچود نیست";
            comboBoxEdit4.Properties.Items.Clear();
            comboBoxEdit4.Text = "موردی موچود نیست";
            comboBoxEdit5.Properties.Items.Clear();
            comboBoxEdit5.Text = "موردی موچود نیست";
            //
            GetCount();
        }
        private void comboBoxEdit1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int index = comboBoxEdit1.SelectedIndex;

                if (index == 0)
                {

                }else
                {
                    index--;
                    String id = item_city_id.GetValue(index).ToString();
                    int cityid = int.Parse(id);
                    city_id = cityid;
                    item_Region_id = db.Regions.Where(c => c.city_id == cityid).Select(c => c.id).ToArray();
                    //Lets Get Items Region
                    Array dd = db.Regions.Where(c => c.city_id == cityid).Select(c => c.name).ToArray();
                    comboBoxEdit2.Text = "";
                    comboBoxEdit2.Properties.Items.Clear();
                    comboBoxEdit2.Text = "...موردی را انتخاب کنید";
                    comboBoxEdit2.Properties.Items.Add("...موردی را انتخاب کنید");
                    comboBoxEdit2.Properties.Items.AddRange(dd);
                    GetCount();
                    Regionid = "";
                    Distractid = "";
                    Rangid = "";
                    Wayid = "";
                    comboBoxEdit3.Properties.Items.Clear();
                    comboBoxEdit3.Text = "موردی موچود نیست";
                    comboBoxEdit4.Properties.Items.Clear();
                    comboBoxEdit4.Text = "موردی موچود نیست";
                    comboBoxEdit5.Properties.Items.Clear();
                    comboBoxEdit5.Text = "موردی موچود نیست";
                }
            }
            catch(Exception ex)
            {

            }

        }
        private void comboBoxEdit2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                int index = comboBoxEdit2.SelectedIndex;
                if (index == 0)
                {

                }
                else
                {
                    index--;
                    Regionid = item_Region_id.GetValue(index).ToString();
                    item_Distract_id = db.Districts.Where(c => c.region_id == Regionid).Select(c => c.id).ToArray();
                    //Lets Get Items Region
                    Array dd = db.Districts.Where(c => c.region_id == Regionid).Select(c => c.name).ToArray();
                    comboBoxEdit3.Text = "";
                    comboBoxEdit3.Properties.Items.Clear();
                    comboBoxEdit3.Text = "...موردی را انتخاب کنید";
                    comboBoxEdit3.Properties.Items.Add("...موردی را انتخاب کنید");
                    comboBoxEdit3.Properties.Items.AddRange(dd);
                    GetCount();
                    //For Clear Other Item
                    Distractid = "";
                    Rangid = "";
                    Wayid = "";
                    comboBoxEdit4.Properties.Items.Clear();
                    comboBoxEdit4.Text = "موردی موچود نیست";
                    comboBoxEdit5.Properties.Items.Clear();
                    comboBoxEdit5.Text = "موردی موچود نیست";
                }
            }
            catch(Exception ex)
            {

            }
            //
        }
        private void comboBoxEdit3_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int index = comboBoxEdit3.SelectedIndex;
                if (index == 0)
                {

                }
                else
                {
                    index--;
                    Distractid = item_Distract_id.GetValue(index).ToString();
                    item_Range_id = db.Ranges.Where(c => c.district_id == Distractid).Select(c => c.id).ToArray();
                    //Lets Get Items Region
                    Array dd = db.Ranges.Where(c => c.district_id == Distractid).Select(c => c.name).ToArray();
                    comboBoxEdit4.Text = "";
                    comboBoxEdit4.Properties.Items.Clear();
                    comboBoxEdit4.Text = "...موردی را انتخاب کنید";
                    comboBoxEdit4.Properties.Items.Add("...موردی را انتخاب کنید");
                    comboBoxEdit4.Properties.Items.AddRange(dd);
                    GetCount();
                    //For Clear Other Item
                    Rangid = "";
                    Wayid = "";
                    comboBoxEdit5.Properties.Items.Clear();
                    comboBoxEdit5.Text = "موردی موچود نیست";
                }
                //
            }
            catch(Exception ex)
            {

            }
        }
        private void comboBoxEdit4_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int index = comboBoxEdit4.SelectedIndex;
                if (index == 0)
                {

                }
                else
                {
                    index--;
                    Rangid = item_Range_id.GetValue(index).ToString();
                    item_Way_id = db.Ways.Where(c => c.rang_id == Rangid).Select(c => c.id).ToArray();
                    //Lets Get Items Region
                    Array dd = db.Ways.Where(c => c.rang_id == Rangid).Select(c => c.name).ToArray();
                    comboBoxEdit5.Text = "";
                    comboBoxEdit5.Properties.Items.Clear();
                    comboBoxEdit5.Text = "...موردی را انتخاب کنید";
                    comboBoxEdit5.Properties.Items.Add("...موردی را انتخاب کنید");
                    comboBoxEdit5.Properties.Items.AddRange(dd);
                    GetCount();
                }
            }
            catch(Exception ex)
            {
              
            }
        }
        private void comboBoxEdit5_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = comboBoxEdit5.SelectedIndex;
            index--;
            Wayid = item_Way_id.GetValue(index).ToString();
            GetCount();
        }
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            //Lets Do This
            MapMainController.Add(city_id, Regionid,Distractid,Rangid,Wayid, Status);
            MapMainController.LoadMarker();

        }
        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            Status = 100;
            GetCount();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            Status = 1;
            GetCount();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            Status = 0;
            GetCount();
        }
        public void GetCount()
        {
            if(Status == 100)
            {
                if (city_id == 0)
                {
                    var getdata = db.Customers;

                    countfilter.Text = getdata.Count().ToString();
                }
                else
                {
                    var getdata = db.Customers.Where(c => c.city_id == city_id && c.region_id.Contains(Regionid) && c.distract_id.Contains(Distractid) && c.rang_id.Contains(Rangid) && c.way_id.Contains(Wayid));
                    countfilter.Text = getdata.Count().ToString();
                }
            }
            else
            {
                if (city_id == 0)
                {
                    var getdata = db.Customers.Where(c => c.status == Status);

                    countfilter.Text = getdata.Count().ToString();
                }
                else
                {
                    var getdata = db.Customers.Where(c => c.city_id == city_id && c.region_id.Contains(Regionid) && c.distract_id.Contains(Distractid) && c.rang_id.Contains(Rangid) && c.way_id.Contains(Wayid) && c.status == Status);
                    countfilter.Text = getdata.Count().ToString();
                }
            }
        }
        #endregion Filters
        //Regigongion
        ArrayList id_list_region = new ArrayList();
        ArrayList city_list_region = new ArrayList();
        //Save
        ArrayList id_list_region_save = new ArrayList();
        ArrayList city_list_region_save = new ArrayList();
        //Districts
        ArrayList id_list_Districts = new ArrayList();
        ArrayList city_list_Districts = new ArrayList();
        //Save
        ArrayList id_list_Districts_save = new ArrayList();
        ArrayList city_list_Districts_save = new ArrayList();
        //Ranges
        ArrayList id_list_Ranges = new ArrayList();
        ArrayList city_list_Ranges = new ArrayList();
        //Save
        ArrayList id_list_Ranges_save = new ArrayList();
        ArrayList city_list_Ranges_save = new ArrayList();
        //Way
        ArrayList id_list_ways = new ArrayList();
        ArrayList city_list_ways = new ArrayList();
        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            int level = e.Node.Level;
           
            if(level == 0)
            {
                e.Node.Nodes.Clear();
                id_list_region.Clear();
                city_list_region.Clear();
                var getid = db.View_1.First(c => c.name == e.Node.Text);
                int id = getid.id;
                var regions= db.Regions.Where(c => c.city_id == id);
                foreach (var item in regions)
                {
                    id_list_region.Add(item.id);
                    city_list_region.Add(item.city_id);
                    e.Node.Nodes.Add(item.name);
                }
            }else if (level == 1)
            {
                e.Node.Nodes.Clear();
                id_list_Districts.Clear();
                city_list_Districts.Clear();
                String id = id_list_region[e.Node.Index].ToString();
                var getdistract = db.Districts.Where(c => c.region_id == id);
                foreach (var item in getdistract)
                {
                    id_list_Districts.Add(item.id);
                    city_list_Districts.Add(item.city_id);
                    e.Node.Nodes.Add(item.name);
                }
            }
            else if (level == 2)
            {
                e.Node.Nodes.Clear();
                id_list_Ranges.Clear();
                city_list_Ranges.Clear();
                String id = id_list_Districts[e.Node.Index].ToString();
                var getdistract = db.Ranges.Where(c => c.district_id == id);
                foreach (var item in getdistract)
                {
                    id_list_Ranges.Add(item.id);
                    city_list_Ranges.Add(item.city_id);
                    e.Node.Nodes.Add(item.name);
                }
            }
            else if (level == 3)
            {
                e.Node.Nodes.Clear();
                id_list_ways.Clear();
                city_list_ways.Clear();
                String id = id_list_Ranges[e.Node.Index].ToString();
                var getdistract = db.Ways.Where(c => c.rang_id == id);
                foreach (var item in getdistract)
                {
                    id_list_ways.Add(item.id);
                    city_list_ways.Add(item.name);
                    e.Node.Nodes.Add(item.name);
                }
            }
        }
        private void barButtonItem3_ItemClick(object sender, ItemClickEventArgs e)
        {
            LoadingController.ShowLoading(splashScreenManager);
            City city = new City();
            LoadingController.HideLoading(splashScreenManager);
            city.ShowDialog();
        }
        private void treeView1_AfterCheck(object sender, TreeViewEventArgs e)
        {
            
        }
        private void gMap_OnMarkerClick(GMap.NET.WindowsForms.GMapMarker item, MouseEventArgs e)
        {
            String id = item.Tag.ToString();
            customer_id.Text = id;
            MapMainController.GetDataMember(int.Parse(id), name, bisiness, Saller_id,saller_name,home_number,home_number_2,phone_number,
                phone_number_2,national_code,postal_code,economic_code,address,activity,market,create_at_Faaliat,city,region,distract,rang,way,sell_center_name,
                type_customer,create_at, update_at);
        }
        List<PointLatLng> points = new List<PointLatLng>();
        public void test()
        {
           
            MapRoute route = OpenStreetMapProvider.Instance.GetRoute(points[0],points[1],false,false,15);
            GMapRoute r = new GMapRoute(route.Points, "Myroutes");
            GMapOverlay routesOverlay = new GMapOverlay("Myroutes");
            routesOverlay.Routes.Add(r);
            //gMap.Overlays.Add(routesOverlay);
            MessageBox.Show("");
        }
        private void نمایشمسیرToolStripMenuItem_Click(object sender, EventArgs e)
        {
            addRoute();
        }
        PointLatLng start;
        PointLatLng startcatch;
        private void gMap_OnMapClick(PointLatLng pointClick, MouseEventArgs e)
        {
          
        }

        private void barEditItem1_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void barEditItem1_EditValueChanged(object sender, EventArgs e)
        {
            BarEditItem item = sender as BarEditItem;
            string newValue = item.EditValue.ToString();
            //gMap.Zoom = int.Parse(newValue);
          
        }

        private void gMap_Load(object sender, EventArgs e)
        {

        }

        private void gMap_OnMapZoomChanged()
        {
            //barEditItem1.EditValue = gMap.Zoom;
        }

        private void checkedComboBoxEdit1_EditValueChanged(object sender, EventArgs e)
        {
            
        }

        private void ribbon_Click(object sender, EventArgs e)
        {

        }

        private void address_Click(object sender, EventArgs e)
        {

        }

        private void barButtonItem4_ItemClick(object sender, ItemClickEventArgs e)
        {
            //Lets Go This
        }

        private void customer_id_Click(object sender, EventArgs e)
        {

        }

        private void name_Click(object sender, EventArgs e)
        {

        }
        //
        public void LoadCustomer()
        {
            db.CustomerDatas.ToList();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
 
            //gMap.Overlays.Clear();
        }

        private void barButtonItem7_ItemClick(object sender, ItemClickEventArgs e)
        {
            Classification classs = new Classification();
            classs.ShowDialog();
        }

        private void گ_ItemClick(object sender, ItemClickEventArgs e)
        {
            Reports repost = new Reports();
            repost.ShowDialog();
        }

        private void groupControl1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tabNavigationPage1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void قراردادنموقعیتشروعToolStripMenuItem_Click(object sender, EventArgs e)
        {
            start = startcatch;
            //
          
            GMapOverlay markers = new GMapOverlay("markers");
            GMapMarker marker = new GMarkerGoogle(start, GMarkerGoogleType.green);
            marker.Tag = "start";
            //marker.ToolTipText = tooltip;
            markers.Markers.Add(marker);
           // gMap.Overlays.Add(markers);
            //:D For Make Start Point
            //RouteController.points = MapMainController.lists;
            //RouteController.ids = MapMainController.id;
            //RouteController.Add(start);
        }

        private void gMap_OnMapClick_1(PointLatLng pointClick, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    
                    break;

                case MouseButtons.Right:
                    //startcatch = pointClick;
                    
                    break;
            }
        }
        GMapOverlay markers;
        PointLatLng _pointClick;
        private void gMap_OnMapDoubleClick(PointLatLng pointClick, MouseEventArgs e)
        {
            this._pointClick = pointClick;
            listBox1.Items.Add("موقعیت شروع ثبت شد");
        }

        private void gMap_MouseDown(object sender, MouseEventArgs e)
        {
           
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        GMapOverlay markers1;
        private void simpleButton3_Click(object sender, EventArgs e)
        {
            
        }
        public void Prepare()
        {
            //
            //RouteController.Show(gMap);
        }

        private void تستToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            //MapRoute route = OpenStreetMapProvider.Instance.GetRoute(startp, endp, false, false, 15);
            //GMapRoute r = new GMapRoute(route.Points, "Myroutes");
            //GMapOverlay routesOverlay = new GMapOverlay("Myroutes");
            //routesOverlay.Routes.Add(r);
            //gMap.Overlays.Add(routesOverlay);
            //r.Stroke.Width = 2;
           

        }

        private void barButtonItem8_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void ل_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void barButtonItem9_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void barButtonItem10_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void menu_region_ItemClick(object sender, ItemClickEventArgs e)
        {
            LoadingController.ShowLoading(splashScreenManager);
            Regions regionf = new Regions();
            LoadingController.HideLoading(splashScreenManager);
            regionf.ShowDialog();
        }

        private void checkEdit3_CheckedChanged(object sender, EventArgs e)
        {
            if(checkEdit3.Checked == true)
            {
                try
                {
                    item_Distract_id = db.Districts.Select(c => c.id).ToArray();
                    //Lets Get Items Region
                    Array dd = db.Districts.Select(c => c.name).ToArray();
                    comboBoxEdit3.Text = "";
                    comboBoxEdit3.Properties.Items.Clear();
                    comboBoxEdit3.Text = "...موردی را انتخاب کنید";
                    comboBoxEdit3.Properties.Items.Add("...موردی را انتخاب کنید");
                    comboBoxEdit3.Properties.Items.AddRange(dd);
                    GetCount();
                    //For Clear Other Item
                    Distractid = "";
                    Rangid = "";
                    Wayid = "";
                    comboBoxEdit4.Properties.Items.Clear();
                    comboBoxEdit4.Text = "موردی موچود نیست";
                    comboBoxEdit5.Properties.Items.Clear();
                    comboBoxEdit5.Text = "موردی موچود نیست";

                }
                catch (Exception ex)
                {

                }
            }
            else
            {

            }
        }

        private void checkEdit2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit2.Checked == true)
            {
                try
                {
                    item_Range_id = db.Ranges.Select(c => c.id).ToArray();
                    //Lets Get Items Region
                    Array dd = db.Ranges.Select(c => c.name).ToArray();
                    comboBoxEdit4.Text = "";
                    comboBoxEdit4.Properties.Items.Clear();
                    comboBoxEdit4.Text = "...موردی را انتخاب کنید";
                    comboBoxEdit4.Properties.Items.Add("...موردی را انتخاب کنید");
                    comboBoxEdit4.Properties.Items.AddRange(dd);
                    GetCount();
                    //For Clear Other Item
                    

                }
                catch (Exception ex)
                {

                }
            }
            else
            {

            }
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit1.Checked == true)
            {
                try
                {
                    item_Way_id = db.Ways.Select(c => c.id).ToArray();
                    //Lets Get Items Region
                    Array dd = db.Ways.Select(c => c.name).ToArray();
                    comboBoxEdit5.Text = "";
                    comboBoxEdit5.Properties.Items.Clear();
                    comboBoxEdit5.Text = "...موردی را انتخاب کنید";
                    comboBoxEdit5.Properties.Items.Add("...موردی را انتخاب کنید");
                    comboBoxEdit5.Properties.Items.AddRange(dd);
                    GetCount();
                    //For Clear Other Item


                }
                catch (Exception ex)
                {

                }
            }
            else
            {

            }
        }

        private void barButtonItem6_ItemClick(object sender, ItemClickEventArgs e)
        {
           
        }

        private void dockPanel2_Click(object sender, EventArgs e)
        {

        }

        public void InitializeChromium()
        {
            CefSettings settings = new CefSettings();
            // Initialize cef with the provided settings
            Cef.Initialize(settings);
            // Create a browser component
            chromeBrowser = new ChromiumWebBrowser("https://firstdata.ir/map/index.html");
            // Add it to the form and fill it to the form window.
            dockPanel2.Controls.Add(chromeBrowser);
            chromeBrowser.Dock = DockStyle.Fill;
            chromeBrowser.MenuHandler = new MyCustomMenuHandler();
            //

        }
        public static String sc = "";
        public static void AddPint(String lat,String lng,String id,String title,String info)
        {
            String po = "addPoint(" + lat + "," + lng + "," + id +",\"" + title +"\",\"" + info +"\")";
            chromeBrowser.ExecuteScriptAsyncWhenPageLoaded(po);
            //51.456871,35.732579;51.300659,35.661759;51.156371,35.032579
            sc = sc + lng + "," + lat + ";";
        }
        public static void addRoute()
        {
            //addRoute('51.456871,35.732579;51.300659,35.661759;51.156371,35.032579')
            sc = sc.Remove(sc.Length - 1);
            Console.WriteLine(sc);
            String jssc = "addRoute('" + sc + "')";
            chromeBrowser.ExecuteScriptAsyncWhenPageLoaded(jssc);
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            addRoute();
        }
    }
}