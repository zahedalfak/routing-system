﻿using System;
using System.Data;
using ExcelDataReader;
using System.Windows.Forms;
using System.IO;
using RoutingSystems.Controller;
using System.Threading;
using RoutingSystems.Model;

namespace RoutingSystems.View
{
    public partial class ExcelImport : DevExpress.XtraEditors.XtraForm
    {
        public ExcelImport()
        {
            InitializeComponent();
        }
        DataTableCollection tableCollection;
        private void btnbrowse_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog() { Filter = "Excel 97-2003 Workbook |*.xls|Excel Workbook|*.xlsx" })
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    txtfilenamexls.Text = openFileDialog.FileName;
                    using (var stream = File.Open(openFileDialog.FileName, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader reader = ExcelReaderFactory.CreateReader(stream))
                        {
                            DataSet result = reader.AsDataSet(new ExcelDataSetConfiguration()
                            {
                            ConfigureDataTable = (_) => new ExcelDataTableConfiguration() { UseHeaderRow = true }
                            });
                            tableCollection = result.Tables;
                            ComboSheet.Properties.Items.Clear();
                            foreach (DataTable table in tableCollection)
                            ComboSheet.Properties.Items.Add(table.TableName);
                        }
                    }
                }
            }
        }
        ImportExcelController ImportExcelController;
        private void ExcelImport_Load(object sender, EventArgs e)
        {
            ImportExcelController = new ImportExcelController();
            ImportExcelController.ShowData(gridControl1);
        }
        DataTable dt;
        Thread thr;
        Thread thr2;
        private void Btnimport_Click(object sender, EventArgs e)
        {
            DataTable _dt = tableCollection[ComboSheet.SelectedItem.ToString()];
            this.dt = _dt;
            int full_count = dt.Rows.Count;
            LabEnd.Text = full_count.ToString();
            BtnReset.Enabled = false;
            BtnSample.Enabled = false;
            btnbrowse.Enabled = false;
            BtnSaveData.Enabled = false;
            Btnimport.Enabled = false;
            ComboSheet.Enabled = false;
            LoadingController.ShowLoading(splashScreenManager1);
            thr = new Thread(insert);
            thr.Start();
        }
        private void insert()
        {
            city = int.Parse( dt.Rows[0]["City"].ToString());
            if (dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ImportExcelController.InsertCatch(
                    dt.Rows[i]["CustomerID"].ToString(),
                    dt.Rows[i]["CustomerName"].ToString(),
                    dt.Rows[i]["CustomerBusinessName"].ToString(),
                    dt.Rows[i]["SellerCode"].ToString(),
                    dt.Rows[i]["SellerName"].ToString(),
                    dt.Rows[i]["PhoneNumber"].ToString(),
                    dt.Rows[i]["PhoneNumber2"].ToString(),
                    dt.Rows[i]["MobileNumber"].ToString(),
                    dt.Rows[i]["NationalCode"].ToString(),
                    dt.Rows[i]["PostalCode"].ToString(),
                    dt.Rows[i]["EconomicCode"].ToString(),
                    dt.Rows[i]["Address"].ToString(),
                    dt.Rows[i]["Longitude"].ToString(),
                    dt.Rows[i]["Latitude"].ToString(),
                    dt.Rows[i]["Status"].ToString(),
                    dt.Rows[i]["Activity"].ToString(),
                    dt.Rows[i]["Market"].ToString(),
                    dt.Rows[i]["Openingdate"].ToString(),
                    dt.Rows[i]["Region"].ToString(),
                    dt.Rows[i]["RegionName"].ToString(),
                    dt.Rows[i]["District"].ToString(),
                    dt.Rows[i]["DistrictName"].ToString(),
                    dt.Rows[i]["Range"].ToString(),
                    dt.Rows[i]["RangeName"].ToString(),
                    dt.Rows[i]["Way"].ToString(),
                    dt.Rows[i]["WayName"].ToString(),
                    dt.Rows[i]["OrganizationalUnit"].ToString(),
                    dt.Rows[i]["Organizationunitname"].ToString(),
                    dt.Rows[i]["Clientclass"].ToString(),
                    dt.Rows[i]["SalesCenterCode"].ToString(),
                    dt.Rows[i]["SalesCenterName"].ToString(),
                    dt.Rows[i]["Customertype"].ToString(),
                    dt.Rows[i]["Description"].ToString()
                        );
                    this.Invoke(new Action(() => end()));
                }
                this.Invoke(new Action(() => EndAll()));
            }
        }
        public void end()
        {
            LabStart.Text = ImportExcelController.Count().ToString();
        }
        public void EndAll()
        {
            LoadingController.HideLoading(splashScreenManager1);
            ImportExcelController.ShowData(gridControl1);
            thr.Abort();
            BtnReset.Enabled = true;
            BtnSample.Enabled = true;
            btnbrowse.Enabled = true;
            BtnSaveData.Enabled = true;
            Btnimport.Enabled = true;
            ComboSheet.Enabled = true;
        }
        int city = 0;
        int count;
        rasharoutesystemEntities1 db = new rasharoutesystemEntities1();
        private void BtnSaveData_Click(object sender, EventArgs e)
        {
            city = int.Parse(textBox1.Text);
            LoadingController.ShowLoading(splashScreenManager1);
             count = gridView1.RowCount;

            progressBarControl1.Properties.Maximum = count;
            progressBarControl1.Properties.Minimum = 0;
            //5 
            int contfull = gridView1.RowCount;
            //17432 / 5 ;
            //
            thr2 = new Thread(test);
            thr2.Start();
            LoadingController.HideLoading(splashScreenManager1);
        }
        public void test()
        {
            int count = gridView1.RowCount;
            for (int i = 0; i < count; i++)
            {
                int id = int.Parse(gridView1.GetRowCellValue(i, "CustomerID").ToString());
                String fullname = gridView1.GetRowCellValue(i, "CustomerName").ToString();
                int business_id = ImportExcelController.CheckNameBusiness(gridView1.GetRowCellValue(i, "CustomerBusinessName").ToString());
                int seller_id;
                if (gridView1.GetRowCellValue(i, "SellerCode").ToString() == "")
                {
                    seller_id = 9999999;
                }
                else
                {
                    seller_id = int.Parse(gridView1.GetRowCellValue(i, "SellerCode").ToString());
                    ImportExcelController.CheckSeller(int.Parse(gridView1.GetRowCellValue(i, "SellerCode").ToString()), gridView1.GetRowCellValue(i, "SellerName").ToString());
                }
                String home_number = gridView1.GetRowCellValue(i, "PhoneNumber").ToString();
                String home_number_2 = gridView1.GetRowCellValue(i, "PhoneNumber2").ToString();
                String phone_number = gridView1.GetRowCellValue(i, "MobileNumber").ToString();
                String phone_number_2 = "";
                String national_code = gridView1.GetRowCellValue(i, "NationalCode").ToString();
                String postal_code = gridView1.GetRowCellValue(i, "PostalCode").ToString();
                String economic_code = gridView1.GetRowCellValue(i, "EconomicCode").ToString();
                String address = gridView1.GetRowCellValue(i, "Address").ToString();
                //Latitude 
                Double lat_value = Double.Parse(gridView1.GetRowCellValue(i, "Latitude").ToString());
                Double long_value = Double.Parse(gridView1.GetRowCellValue(i, "Longitude").ToString());
                String status = gridView1.GetRowCellValue(i, "Status").ToString();
                byte j = 1;
                if (status == "فعال")
                {
                    j = 1;
                    status = "true";
                }
                else
                {
                    j = 0;
                    status = "false";
                }
                //bool st = Boolean.Parse(status);
                int activity_id = ImportExcelController.CheckActivity(gridView1.GetRowCellValue(i, "Activity").ToString());
                int market_id = ImportExcelController.CheckMarket(gridView1.GetRowCellValue(i, "Market").ToString());
                String create_at_persian = gridView1.GetRowCellValue(i, "Openingdate").ToString();
                int city_id = city;
                string region_id = ImportExcelController.CheckRegion(gridView1.GetRowCellValue(i, "Region").ToString(), gridView1.GetRowCellValue(i, "RegionName").ToString(), city);
                int region_code = ImportExcelController.GetRegionId(gridView1.GetRowCellValue(i, "Region").ToString(), gridView1.GetRowCellValue(i, "RegionName").ToString(), city);
                string distract_id = ImportExcelController.CheckDistract(gridView1.GetRowCellValue(i, "District").ToString(), gridView1.GetRowCellValue(i, "DistrictName").ToString(), city, region_id);
                int distract_code = ImportExcelController.GetDistractId(gridView1.GetRowCellValue(i, "District").ToString(), gridView1.GetRowCellValue(i, "DistrictName").ToString(), city, region_id);
                string rang_id = ImportExcelController.CheckRang(gridView1.GetRowCellValue(i, "Range").ToString(), gridView1.GetRowCellValue(i, "RangeName").ToString(), city, distract_id);
                int rang_code = ImportExcelController.GetRangId(gridView1.GetRowCellValue(i, "Range").ToString(), gridView1.GetRowCellValue(i, "RangeName").ToString(), city, distract_id);
                string way_id = ImportExcelController.CheckWay(gridView1.GetRowCellValue(i, "Way").ToString(), gridView1.GetRowCellValue(i, "WayName").ToString(), city_id, rang_id);
                int way_code = ImportExcelController.GetWayId(gridView1.GetRowCellValue(i, "Way").ToString(), gridView1.GetRowCellValue(i, "WayName").ToString(), city_id, rang_id);
                int city_code = city;
                String classs = gridView1.GetRowCellValue(i, "Clientclass").ToString();
                int sell_center_code = ImportExcelController.CheckSellCenterCode(gridView1.GetRowCellValue(i, "SalesCenterCode").ToString());
                String sell_center_name = gridView1.GetRowCellValue(i, "SalesCenterName").ToString();
                int customer_type_id = ImportExcelController.CheckCustomerType(gridView1.GetRowCellValue(i, "Customertype").ToString());
                String description = gridView1.GetRowCellValue(i, "Description").ToString();
                int Organizational_id = ImportExcelController.CheckOrganizational(gridView1.GetRowCellValue(i, "Organizationunitname").ToString());
                Customer customer = new Customer()
                {
                    id = id,
                    fullname = fullname,
                    business_id = business_id,
                    seller_id = seller_id,
                    home_number = home_number,
                    home_number_2 = home_number_2,
                    phone_number = phone_number,
                    phone_number_2 = phone_number_2,
                    national_code = national_code,
                    postal_code = postal_code,
                    economic_code = economic_code,
                    address = address,
                    lat = lat_value,
                    @long = long_value,
                    status = j,
                    activity_id = activity_id,
                    market_id = market_id,
                    create_at_persian = create_at_persian,
                    create_at_england = shamsi2miladi.shamsitomiladi(create_at_persian),
                    city_id = city_id,
                    city_code = city_code,
                    region_id = region_id,
                    region_code = region_code,
                    distract_id = distract_id,
                    distract_code = distract_code,
                    rang_id = rang_id,
                    range_code = rang_code,
                    way_id = way_id,
                    way_code = way_code,
                    classs = classs,
                    sell_center_code = sell_center_code,
                    sell_center_name = sell_center_name,
                    customer_type_id = customer_type_id,
                    description = description,
                    create_at = DateTime.Now,
                    update_at = DateTime.Now,
                    Organizational_id = Organizational_id,
                    admin_insert_id = 1
                };
                db.Customers.Add(customer);
                db.SaveChanges();
                //this.Invoke(new Action(() => SetValue()));
            }
            this.Invoke(new Action(() => Finish()));
        }
        private void SetValue()
        {
            int step = progressBarControl1.Properties.Step;
            int stepplus = step + 1;
            progressBarControl1.Properties.Step = stepplus;
        }
        private void Finish()
        {
            ImportExcelController.Clear();
            ImportExcelController.ShowData(gridControl1);

        }
        private void BtnReset_Click(object sender, EventArgs e)
        {
            ImportExcelController.Clear();
            ImportExcelController.ShowData(gridControl1);
            LabStart.Text = "0";
            LabEnd.Text = "0";
            ComboSheet.Text = "";
            progressBarControl1.Properties.Step = 0;
        }
        private void BtnSample_Click(object sender, EventArgs e)
        {
            FileInfo fi = new FileInfo(Application.StartupPath + "\\Data\\Sample.xls");
            if (fi.Exists)
            {
                System.Diagnostics.Process.Start(Application.StartupPath + "\\Data\\Sample.xls");
            }
            else
            {
                //file doesn't exist
            }
        }
    }
}