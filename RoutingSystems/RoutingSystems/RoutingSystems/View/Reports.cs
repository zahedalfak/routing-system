﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RoutingSystems.Controller;

namespace RoutingSystems.View
{
    public partial class Reports : DevExpress.XtraEditors.XtraForm
    {
        ReportController ReportController;
        public Reports()
        {
            InitializeComponent();
        }

        private void Reports_Load(object sender, EventArgs e)
        {
            ReportController = new ReportController();
            countcustomer.Text = ReportController.GetCustomerCoumt().ToString();
            coumtbessiness.Text = ReportController.GetBissinessCount().ToString();
            countactivity.Text = ReportController.GetActivityCount().ToString();
            countcity.Text = ReportController.GetCitysCount().ToString();
            countmarket.Text = ReportController.GetMarketCount().ToString();
        }

        private void countcustomer_Click(object sender, EventArgs e)
        {

        }
    }
}
