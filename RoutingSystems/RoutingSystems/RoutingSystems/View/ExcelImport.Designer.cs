﻿namespace RoutingSystems.View
{
    partial class ExcelImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.progressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            this.BtnSaveData = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabEnd = new System.Windows.Forms.Label();
            this.LabStart = new System.Windows.Forms.Label();
            this.ComboSheet = new DevExpress.XtraEditors.ComboBoxEdit();
            this.BtnSample = new DevExpress.XtraEditors.SimpleButton();
            this.Btnimport = new DevExpress.XtraEditors.SimpleButton();
            this.BtnReset = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.btnbrowse = new DevExpress.XtraEditors.SimpleButton();
            this.txtfilenamexls = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::RoutingSystems.View.Wait), true, true);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboSheet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfilenamexls.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.textBox1);
            this.groupControl1.Controls.Add(this.progressBarControl1);
            this.groupControl1.Controls.Add(this.BtnSaveData);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.LabEnd);
            this.groupControl1.Controls.Add(this.LabStart);
            this.groupControl1.Controls.Add(this.ComboSheet);
            this.groupControl1.Controls.Add(this.BtnSample);
            this.groupControl1.Controls.Add(this.Btnimport);
            this.groupControl1.Controls.Add(this.BtnReset);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.btnbrowse);
            this.groupControl1.Controls.Add(this.txtfilenamexls);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupControl1.Location = new System.Drawing.Point(0, 307);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(969, 107);
            this.groupControl1.TabIndex = 2;
            this.groupControl1.Text = "اقدامات";
            // 
            // progressBarControl1
            // 
            this.progressBarControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarControl1.Location = new System.Drawing.Point(257, 81);
            this.progressBarControl1.Name = "progressBarControl1";
            this.progressBarControl1.Size = new System.Drawing.Size(484, 23);
            this.progressBarControl1.TabIndex = 15;
            // 
            // BtnSaveData
            // 
            this.BtnSaveData.Location = new System.Drawing.Point(5, 81);
            this.BtnSaveData.Name = "BtnSaveData";
            this.BtnSaveData.Size = new System.Drawing.Size(115, 23);
            this.BtnSaveData.TabIndex = 14;
            this.BtnSaveData.Text = "ذخیره اطلاعات";
            this.BtnSaveData.ToolTip = "اطلاعات را در پایگاه داده ذخیره کنید.";
            this.BtnSaveData.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSaveData.Click += new System.EventHandler(this.BtnSaveData_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl4.Location = new System.Drawing.Point(825, 86);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(7, 13);
            this.labelControl4.TabIndex = 13;
            this.labelControl4.Text = "از";
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl3.Location = new System.Drawing.Point(901, 86);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(56, 13);
            this.labelControl3.TabIndex = 12;
            this.labelControl3.Text = "تعداد موارد :";
            // 
            // LabEnd
            // 
            this.LabEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LabEnd.AutoSize = true;
            this.LabEnd.Location = new System.Drawing.Point(788, 87);
            this.LabEnd.Name = "LabEnd";
            this.LabEnd.Size = new System.Drawing.Size(13, 13);
            this.LabEnd.TabIndex = 11;
            this.LabEnd.Text = "0";
            // 
            // LabStart
            // 
            this.LabStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LabStart.AutoSize = true;
            this.LabStart.Location = new System.Drawing.Point(852, 87);
            this.LabStart.Name = "LabStart";
            this.LabStart.Size = new System.Drawing.Size(13, 13);
            this.LabStart.TabIndex = 10;
            this.LabStart.Text = "0";
            // 
            // ComboSheet
            // 
            this.ComboSheet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ComboSheet.Location = new System.Drawing.Point(612, 55);
            this.ComboSheet.Name = "ComboSheet";
            this.ComboSheet.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboSheet.Size = new System.Drawing.Size(250, 20);
            this.ComboSheet.TabIndex = 9;
            // 
            // BtnSample
            // 
            this.BtnSample.Location = new System.Drawing.Point(126, 81);
            this.BtnSample.Name = "BtnSample";
            this.BtnSample.Size = new System.Drawing.Size(115, 23);
            this.BtnSample.TabIndex = 8;
            this.BtnSample.Text = "دریافت فایل نمونه";
            this.BtnSample.ToolTip = "فایل نمونه برای درون ریزی از اکسل را دریافت کنید.";
            this.BtnSample.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSample.Click += new System.EventHandler(this.BtnSample_Click);
            // 
            // Btnimport
            // 
            this.Btnimport.Location = new System.Drawing.Point(5, 52);
            this.Btnimport.Name = "Btnimport";
            this.Btnimport.Size = new System.Drawing.Size(115, 23);
            this.Btnimport.TabIndex = 7;
            this.Btnimport.Text = "ورود اطلاعات";
            this.Btnimport.ToolTip = "اطلاعات را بارگذاری کنید .";
            this.Btnimport.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.Btnimport.Click += new System.EventHandler(this.Btnimport_Click);
            // 
            // BtnReset
            // 
            this.BtnReset.Location = new System.Drawing.Point(126, 53);
            this.BtnReset.Name = "BtnReset";
            this.BtnReset.Size = new System.Drawing.Size(115, 23);
            this.BtnReset.TabIndex = 6;
            this.BtnReset.Text = "بازنویسی";
            this.BtnReset.ToolTip = "تمامی المان های فرم خالی خواهند شد .";
            this.BtnReset.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Warning;
            this.BtnReset.Click += new System.EventHandler(this.BtnReset_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl2.Location = new System.Drawing.Point(893, 58);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(64, 13);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "انتخاب شیت :";
            // 
            // btnbrowse
            // 
            this.btnbrowse.Location = new System.Drawing.Point(5, 23);
            this.btnbrowse.Name = "btnbrowse";
            this.btnbrowse.Size = new System.Drawing.Size(115, 23);
            this.btnbrowse.TabIndex = 2;
            this.btnbrowse.Text = "انتخاب اکسل";
            this.btnbrowse.ToolTip = "فایل اکسل مورد نظر را انتخاب کنید .";
            this.btnbrowse.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Question;
            this.btnbrowse.Click += new System.EventHandler(this.btnbrowse_Click);
            // 
            // txtfilenamexls
            // 
            this.txtfilenamexls.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtfilenamexls.Enabled = false;
            this.txtfilenamexls.Location = new System.Drawing.Point(126, 23);
            this.txtfilenamexls.Name = "txtfilenamexls";
            this.txtfilenamexls.Properties.AutoHeight = false;
            this.txtfilenamexls.Properties.LookAndFeel.SkinName = "Office 2016 Colorful";
            this.txtfilenamexls.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.txtfilenamexls.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtfilenamexls.Size = new System.Drawing.Size(736, 23);
            this.txtfilenamexls.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.Location = new System.Drawing.Point(868, 27);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(89, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "انتخاب فایل اکسل :";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(969, 307);
            this.gridControl1.TabIndex = 3;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // splashScreenManager1
            // 
            this.splashScreenManager1.ClosingDelay = 500;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(247, 54);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 21);
            this.textBox1.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(353, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "کد شهر";
            // 
            // ExcelImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 414);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.groupControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ExcelImport";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ورود اطلاعات از اکسل";
            this.Load += new System.EventHandler(this.ExcelImport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboSheet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtfilenamexls.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton BtnSample;
        private DevExpress.XtraEditors.SimpleButton Btnimport;
        private DevExpress.XtraEditors.SimpleButton BtnReset;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton btnbrowse;
        private DevExpress.XtraEditors.TextEdit txtfilenamexls;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.ComboBoxEdit ComboSheet;
        private System.Windows.Forms.Label LabStart;
        private System.Windows.Forms.Label LabEnd;
        private DevExpress.XtraEditors.SimpleButton BtnSaveData;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private DevExpress.XtraEditors.ProgressBarControl progressBarControl1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
    }
}