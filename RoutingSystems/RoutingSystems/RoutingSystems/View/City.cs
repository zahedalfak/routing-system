﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using RoutingSystems.Controller;
using System.Data.Entity;

namespace RoutingSystems.View
{
    public partial class City : DevExpress.XtraEditors.XtraForm
    {
        CityController cityController;
        public City()
        {
            InitializeComponent();
        }
        private void City_Load(object sender, EventArgs e)
        {
           cityController = new CityController();
           cityController.GetAll(gridControl1);
        }
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            cityController.Insert(textEdit1.Text);
            cityController.GetAll(gridControl1);
        }
        private void حذفToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try { 
            
                int id = int.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "id").ToString());
                Boolean check = cityController.Check(id);
                if (check == true)
                {
                    MessageBox.Show("مشتری برای این شهر ثبت شده است");
                }
                else
                {
                    cityController.Delete(id);
                    cityController.GetAll(gridControl1);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            try
            {

                int id = int.Parse(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "id").ToString());
                Boolean check = cityController.Check(id);
                if (check == true)
                {
                    MessageBox.Show("مشتری برای این شهر ثبت شده است");
                }
                else
                {
                    String name = gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "name").ToString();
                    cityController.Update(id, name);
                    cityController.GetAll(gridControl1);
                    MessageBox.Show("انجام شد");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}