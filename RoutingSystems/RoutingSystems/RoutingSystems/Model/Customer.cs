//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RoutingSystems.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Customer
    {
        public int id { get; set; }
        public string fullname { get; set; }
        public Nullable<int> business_id { get; set; }
        public Nullable<int> seller_id { get; set; }
        public string home_number { get; set; }
        public string home_number_2 { get; set; }
        public string phone_number { get; set; }
        public string phone_number_2 { get; set; }
        public string national_code { get; set; }
        public string postal_code { get; set; }
        public string economic_code { get; set; }
        public string address { get; set; }
        public Nullable<double> lat { get; set; }
        public Nullable<double> @long { get; set; }
        public Nullable<byte> status { get; set; }
        public Nullable<int> activity_id { get; set; }
        public Nullable<int> market_id { get; set; }
        public string create_at_persian { get; set; }
        public Nullable<System.DateTime> create_at_england { get; set; }
        public Nullable<int> city_id { get; set; }
        public Nullable<int> city_code { get; set; }
        public string region_id { get; set; }
        public Nullable<int> region_code { get; set; }
        public string distract_id { get; set; }
        public Nullable<int> distract_code { get; set; }
        public string rang_id { get; set; }
        public Nullable<int> range_code { get; set; }
        public string way_id { get; set; }
        public Nullable<int> way_code { get; set; }
        public string classs { get; set; }
        public Nullable<int> sell_center_code { get; set; }
        public string sell_center_name { get; set; }
        public Nullable<int> customer_type_id { get; set; }
        public string description { get; set; }
        public Nullable<System.DateTime> create_at { get; set; }
        public Nullable<System.DateTime> update_at { get; set; }
        public Nullable<int> Organizational_id { get; set; }
        public Nullable<int> admin_insert_id { get; set; }
    
        public virtual Activity Activity { get; set; }
        public virtual Admin Admin { get; set; }
        public virtual Business Business { get; set; }
        public virtual City City { get; set; }
        public virtual CustomerType CustomerType { get; set; }
        public virtual Market Market { get; set; }
        public virtual Organizational Organizational { get; set; }
        public virtual SellCenterCode SellCenterCode { get; set; }
        public virtual Seller Seller { get; set; }
    }
}
