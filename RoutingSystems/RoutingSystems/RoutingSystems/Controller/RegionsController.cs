﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RoutingSystems.Model;
using DevExpress.XtraGrid;
using System.Windows.Forms;

namespace RoutingSystems.Controller
{
    class RegionsController
    {
        rasharoutesystemEntities1 db = new rasharoutesystemEntities1();
        public int GetData(GridControl data)
        {
            try
            {
                data.DataSource = db.RegionsVs.ToList();
                return 200;
            }
            catch (Exception ex)
            {
                return 500;
            }
        }
        public int Insert(String name,int city_id,String id)
        {
            try
            {
             
                Region insert = new Region()
                {
                    city_id = city_id,
                    name = name,
                    id = id
                };
                db.Regions.Add(insert);
                db.SaveChanges();
                return 200;
            }catch(Exception ex)
            {
               
                return 500;
            }
        }
        public int Delete(int base_id)
        {
            try
            {
                var delete = db.Regions.First(c => c.base_id == base_id);
                db.Regions.Remove(delete);
                db.SaveChanges();
                return 200;
            }catch(Exception ex)
            {
                return 500;
            }
        }
    }
}
