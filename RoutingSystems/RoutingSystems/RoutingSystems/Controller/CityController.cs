﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using RoutingSystems.Model;

namespace RoutingSystems.Controller
{
    class CityController
    {
        rasharoutesystemEntities1 db = new rasharoutesystemEntities1();
        public void GetAll(GridControl data)
        {
            data.DataSource = db.Cities.ToList();
        }
        public void Insert(String name)
        {
            if (name == "")
            {
                MessageBox.Show("نام را وارد نکرده اید");
            }
            else
            {
                City city = new City()
                {
                    name = name
                };
                db.Cities.Add(city);
                db.SaveChanges();
            }
        }
        public void Delete(int id)
        {
            var delete = db.Cities.FirstOrDefault(c => c.id == id);
            db.Cities.Remove(delete);
            db.SaveChanges();
        }
        public Boolean Check(int id)
        {
            var check = db.Customers.Where(c => c.city_id == id);
            if (check.Any())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void Update(int id,String name)
        {
            var update = db.Cities.First(c => c.id == id);
            update.name = name;
            db.SaveChanges();
        }
    }
}
