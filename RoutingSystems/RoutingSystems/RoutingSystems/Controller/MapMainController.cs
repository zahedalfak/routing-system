﻿using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RoutingSystems.Model;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using RoutingSystems.View;

namespace RoutingSystems.Controller
{
    class MapMainController
    {
        public static ArrayList id = new ArrayList();
        ArrayList lat = new ArrayList();
        ArrayList longv = new ArrayList();
        public static List<PointLatLng> lists = new List<PointLatLng>();
        rasharoutesystemEntities1 db = new rasharoutesystemEntities1();
        public void Add(int city_id,String region_id,String distract_id,String rang_id,String way_id,int status)
        {
            if(status == 100)
            {
                var getdata = db.Customers.Where(c => c.city_id == city_id && c.region_id.Contains(region_id) && c.distract_id.Contains(distract_id) && c.rang_id.Contains(rang_id) && c.way_id.Contains(way_id));
                MessageBox.Show(getdata.Count().ToString());
                foreach (var item in getdata)
                {
                    if (item.lat == 0 && item.@long == 0)
                    {
                       
                    }
                    else
                    {
                        
                        id.Add(item.id);
                        lat.Add(item.lat);
                        longv.Add(item.@long);
                        double latv = double.Parse(item.lat.ToString());
                        double _longv = double.Parse(item.@long.ToString());
                        lists.Add(new PointLatLng(latv, _longv));
                        
                    }

                }
            }
            else
            {
                var getdata = db.Customers.Where(c => c.city_id == city_id && c.region_id.Contains(region_id) && c.distract_id.Contains(distract_id) && c.rang_id.Contains(rang_id) && c.way_id.Contains(way_id) && c.status == status);
                foreach (var item in getdata)
                {
                    if (item.lat == 0 && item.@long == 0)
                    {

                    }
                    else
                    {
                        id.Add(item.id);
                        lat.Add(item.lat);
                        longv.Add(item.@long);
                        double latv = double.Parse(item.lat.ToString());
                        double _longv = double.Parse(item.@long.ToString());
                        lists.Add(new PointLatLng(latv, _longv));
                        ;
                    }

                }
            }
        }
        public void Remove(String region_id, String distract_id, String rang_id, String way_id)
        {
            var getdata = db.Customers.Where(c => c.region_id.Contains(region_id) && c.distract_id.Contains(distract_id) && c.rang_id.Contains(rang_id) && c.way_id.Contains(way_id));
            foreach (var item in getdata)
            {
                if (item.lat == 0 && item.@long == 0)
                {

                }
                else
                {
                    id.Remove(item.id);
                    lat.Remove(item.lat);
                    longv.Remove(item.@long);
                    double latv = double.Parse(item.lat.ToString());
                    double _longv = double.Parse(item.@long.ToString());
                    lists.Remove(new PointLatLng(latv, _longv));
                }
            }
        }
        public void LoadMarker()
        {
            int countlat = lat.Count;
            MessageBox.Show(countlat.ToString());
            //gMapControl1.SetZoomToFitRect(new PointLatLng(double.Parse(lat[0].ToString()), double.Parse(longv[0].ToString());
            for (int i = 0; i < countlat; i++)
            {
                double latv =  double.Parse( lat[i].ToString());
                double _longv = double.Parse(longv[i].ToString());
                //marker.Tag = id[i];
                //marker.ToolTipText = tooltip;
                Main.AddPint(latv.ToString(), _longv.ToString(), id[i].ToString(), "", "");
            }
        }
        public void GetDataMember(int id, LabelControl name, LabelControl bisiness, LabelControl Saller_id,
            LabelControl saller_name, LabelControl home_number, LabelControl home_number_2, LabelControl phone_number,
            LabelControl phone_number_2, LabelControl national_code, LabelControl postal_code, LabelControl economic_code,
            LabelControl address, LabelControl activity, LabelControl market, LabelControl create_at_Faaliat, LabelControl city,
            LabelControl region, LabelControl distract, LabelControl rang, LabelControl way, LabelControl sell_center_name,
            LabelControl type_customer, LabelControl create_at, LabelControl update_at)
        {
            var get = db.Customers.First(c => c.id == id);
            name.Text = get.fullname;
            String bissinessname = GetBissinessname(int.Parse(get.business_id.ToString()));
            bisiness.Text = bissinessname;
            Saller_id.Text = get.Seller.id.ToString();
            saller_name.Text = get.Seller.name;
            home_number.Text = get.home_number;
            home_number_2.Text = get.home_number_2;
            phone_number.Text = get.phone_number;
            phone_number_2.Text = get.phone_number_2;
            national_code.Text = get.national_code;
            postal_code.Text = get.postal_code;
            economic_code.Text = get.economic_code;
            address.Text = get.address;
            activity.Text = get.Activity.name;
            market.Text = get.Market.name;
            create_at_Faaliat.Text = get.create_at_persian;
            city.Text = get.City.name;
            ///
            var q1 = db.Regions.First(c => c.base_id == get.region_code);
            region.Text = q1.name;
            //
            var q2 = db.Districts.First(c => c.id_base == get.distract_code);
            distract.Text = q2.name;
            //
            var q3 = db.Ranges.First(c => c.base_id == get.range_code);
            rang.Text = q3.name;
            //
            var q4 = db.Ways.First(c => c.base_id == get.way_code);
            way.Text = q4.name;
            //region.Text = get.region_code;
            sell_center_name.Text = get.sell_center_name;
            type_customer.Text = get.CustomerType.name;
        }

        public String GetBissinessname(int id)
        {
            try
            {
                var get = db.Businesses.First(c => c.id == id);
                return get.name;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
    }
}
