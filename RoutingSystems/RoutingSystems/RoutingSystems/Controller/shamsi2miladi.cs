﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoutingSystems.Controller
{
    class shamsi2miladi
    {
        public static string dat, sal, mah, roz;
        public static DateTime shamsitomiladi(string s)
        {
            dat = s;
            sal = dat.Substring(0, 4);
            mah = dat.Substring(5, 2);
            roz = dat.Substring(8, 2);
            PersianCalendar pc = new PersianCalendar();
            return pc.ToDateTime(Convert.ToInt32(sal), Convert.ToInt32(mah), Convert.ToInt32(roz), 0, 0, 0, 0);
        }
    }
}
