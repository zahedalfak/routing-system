﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using RoutingSystems.Model;

namespace RoutingSystems.Controller
{

    class ImportExcelController
    {
        rasharoutesystemEntities1 db = new rasharoutesystemEntities1();
        public void InsertCatch(String CustomerID, String CustomerName, String CustomerBusinessName, String SellerCode,
            String SellerName, String PhoneNumber, String PhoneNumber2, String MobileNumber, String NationalCode,
            String PostalCode, String EconomicCode, String Address, String Longitude, String Latitude,
            String Status, String Activity, String Market, String Openingdate, String Region,
            String RegionName, String District, String DistrictName, String Range, String RangeName, String Way, String WayName, String OrganizationalUnit, String Organizationunitname,
            String Clientclass, String SalesCenterCode, String SalesCenterName, String Customertype, String Description
            )
        {
            try
            {
                CacheData catchinsert = new CacheData()
                {
                    CustomerID = CustomerID,
                    CustomerName = CustomerName,
                    CustomerBusinessName = CustomerBusinessName,
                    SellerCode = SellerCode,
                    SellerName = SellerName,
                    PhoneNumber = PhoneNumber,
                    PhoneNumber2 = PhoneNumber2,
                    MobileNumber = MobileNumber,
                    NationalCode = NationalCode,
                    PostalCode = PostalCode,
                    EconomicCode = EconomicCode,
                    Address = Address,
                    Longitude = Longitude,
                    Latitude = Latitude,
                    Status = Status,
                    Activity = Activity,
                    Market = Market,
                    Openingdate = Openingdate,
                    Region = Region,
                    RegionName = RegionName,
                    District = District,
                    DistrictName = DistrictName,
                    Range = Range,
                    RangeName = RangeName,
                    Way = Way,
                    WayName = WayName,
                    OrganizationalUnit = OrganizationalUnit,
                    Organizationunitname = Organizationunitname,
                    Clientclass = Clientclass,
                    SalesCenterCode = SalesCenterCode,
                    SalesCenterName = SalesCenterName,
                    Customertype = Customertype,
                    Description = Description
                };
                db.CacheDatas.Add(catchinsert);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public int Count()
        {
            return db.CacheDatas.Count();
        }
        public void ShowData(GridControl data)
        {
            data.DataSource = db.CacheDatas.ToList();
        }
        public int CheckNameBusiness(String name)
        {
            var check = db.Businesses.Where(c => c.name == name);
            if (check.Any())
            {
                return check.First().id;
            }
            else
            {
                Business business = new Business()
                {
                    name = name
                };
                db.Businesses.Add(business);
                db.SaveChanges();
                return db.Businesses.First(c => c.name == name).id;
            }
        }
        public void CheckSeller(int id,String name)
        {
            var check = db.Sellers.Where(c => c.id == id);
            if (check.Any())
            {
                //
            }
            else
            {
                Seller seller = new Seller()
                {
                    id = id,
                    name = name
                };
                db.Sellers.Add(seller);
                db.SaveChanges();
            }
        }
        public int CheckActivity(String name)
        {
            var check = db.Activities.Where(c => c.name == name);
            if (check.Any())
            {
               return check.First().id;
            }
            else
            {
                Activity activity = new Activity()
                {
                   name = name
                };
                db.Activities.Add(activity);
                db.SaveChanges();
                return db.Activities.First(c => c.name == name).id;
            }
        }
        public int CheckMarket(String name)
        {
            var check = db.Markets.Where(c => c.name == name);
            if (check.Any())
            {
                return check.First().id;
            }
            else
            {
                Market market = new Market()
                {
                    name = name
                };
                db.Markets.Add(market);
                db.SaveChanges();
                return db.Markets.First(c => c.name == name).id;
            }
        }
        public int CheckCity(String name)
        {
            var check = db.Cities.Where(c => c.name == name);
            if (check.Any())
            {
                return check.First().id;
            }
            else
            {
                City city = new City()
                {
                    name = name
                };
                db.Cities.Add(city);
                db.SaveChanges();
                return db.Cities.First(c => c.name == name).id;
            }
        }
        public string CheckRegion(String id,String name,int city)
        {
            var check = db.Regions.Where(c => c.name == name);
            if (check.Any())
            {
                return check.First().id;
            }
            else
            {
                Region region = new Region()
                {
                    id=id,
                    name = name,
                    city_id=city
                };
                db.Regions.Add(region);
                db.SaveChanges();
                return db.Regions.First(c => c.name == name).id;
            }
        }
        public int GetRegionId(String id,String name,int city)
        {
            var get = db.Regions.Where(c => c.id == id && c.name == name && c.city_id == city);
            if (get.Any())
            {
                return get.First().base_id;
            }
            else { return 0;  }
        }
        public string CheckDistract(string id,String name,int city_id,String region_id)
        {
            var check = db.Districts.Where(c => c.name == name && c.region_id == region_id);
            if (check.Any())
            {
                return check.First().id;
            }
            else
            {
                District district = new District()
                {
                    id=id,
                    name = name,
                    city_id = city_id,
                    region_id= region_id
                };
                db.Districts.Add(district);
                db.SaveChanges();
                return db.Districts.First(c => c.name == name).id;
            }
        }
        public int GetDistractId(String id, String name, int city, String region_id)
        {
            var get = db.Districts.Where(c => c.id == id && c.name == name && c.city_id == city && c.region_id == region_id);
            if (get.Any())
            {
                return get.First().id_base;
            }
            else { return 0; }
        }
        public string CheckRang(string id,String name,int city_id,string district_id)
        {
            var check = db.Ranges.Where(c => c.name == name && c.district_id == district_id);
            if (check.Any())
            {
                return check.First().id;
            }
            else
            {
                Range range = new Range()
                {
                    id=id,
                    name = name,
                    city_id = city_id,
                    district_id = district_id
                };
                db.Ranges.Add(range);
                db.SaveChanges();
                return db.Ranges.First(c => c.name == name).id;
            }
        }
        public int GetRangId(String id, String name, int city_id, String district_id)
        {
            var get = db.Ranges.Where(c => c.id == id && c.name == name && c.city_id == city_id && c.district_id == district_id);
            if (get.Any())
            {
                return get.First().base_id;
            }
            else { return 0; }
        }
        public string CheckWay(string id,String name,int city_id,string rang_id)
        {
            var check = db.Ways.Where(c => c.name == name && c.rang_id == rang_id);
            if (check.Any())
            {
                return check.First().id;
            }
            else
            {
                Way way = new Way()
                {
                    id = id,
                    name = name,
                    city_id = city_id,
                    rang_id = rang_id
                };
                db.Ways.Add(way);
                db.SaveChanges();
                return db.Ways.First(c => c.name == name).id;
            }
        }
        public int GetWayId(string id, String name, int city_id, string rang_id)
        {
            var get = db.Ways.Where(c => c.id == id && c.name == name && c.city_id == city_id && c.rang_id == rang_id);
            if (get.Any())
            {
                return get.First().base_id;
            }
            else { return 0; }
        }
        public int CheckOrganizational(String name)
        {
            var check = db.Organizationals.Where(c => c.name == name);
            if (check.Any())
            {
                return check.First().id;
            }
            else
            {
                Organizational organizational = new Organizational()
                {
                    name = name
                };
                db.Organizationals.Add(organizational);
                db.SaveChanges();
                return db.Organizationals.First(c => c.name == name).id;
            }
        }        
        public int CheckCustomerType(String name)
        {
            var check = db.CustomerTypes.Where(c => c.name == name);
            if (check.Any())
            {
                return check.First().id;
            }
            else
            {
                CustomerType customerType = new CustomerType()
                {
                    name = name
                };
                db.CustomerTypes.Add(customerType);
                db.SaveChanges();
                return db.CustomerTypes.First(c => c.name == name).id;
            }
        }
        public int CheckSellCenterCode(String code)
        {
            var check = db.SellCenterCodes.Where(c => c.code == code);
            if (check.Any())
            {
                return check.First().id;
            }
            else
            {
                SellCenterCode sellcentercode = new SellCenterCode()
                {
                    code = code
                };
                db.SellCenterCodes.Add(sellcentercode);
                db.SaveChanges();
                return db.SellCenterCodes.First(c => c.code == code).id;
            }
        }
        public void Clear()
        {
            db.DeleteCatchData();
            db.SaveChanges();
        }
    }
}
