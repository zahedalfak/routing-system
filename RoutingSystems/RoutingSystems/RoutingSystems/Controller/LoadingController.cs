﻿
namespace RoutingSystems.Controller
{
    class LoadingController
    {
        public static void ShowLoading(DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager)
        {
            splashScreenManager.ShowWaitForm();
        }
        public static void HideLoading(DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager)
        {
            splashScreenManager.CloseWaitForm();
        }
    }
}
