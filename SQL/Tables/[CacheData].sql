USE [rasharoutesystem]
GO

/****** Object:  Table [dbo].[CacheData]    Script Date: 2/28/2020 6:12:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CacheData](
	[CustomerID] [varchar](50) NOT NULL,
	[CustomerName] [nvarchar](max) NULL,
	[CustomerBusinessName] [nvarchar](max) NULL,
	[SellerCode] [varchar](50) NULL,
	[SellerName] [nvarchar](max) NULL,
	[PhoneNumber] [varchar](50) NULL,
	[PhoneNumber2] [varchar](50) NULL,
	[MobileNumber] [varchar](50) NULL,
	[NationalCode] [varchar](50) NULL,
	[PostalCode] [varchar](50) NULL,
	[EconomicCode] [varchar](50) NULL,
	[Address] [nvarchar](max) NULL,
	[Longitude] [varchar](50) NULL,
	[Latitude] [varchar](50) NULL,
	[Status] [nvarchar](max) NULL,
	[Activity] [nvarchar](max) NULL,
	[Market] [nvarchar](max) NULL,
	[Openingdate] [nvarchar](max) NULL,
	[Region] [nvarchar](max) NULL,
	[RegionName] [nvarchar](max) NULL,
	[District] [nvarchar](max) NULL,
	[DistrictName] [nvarchar](max) NULL,
	[Range] [nvarchar](max) NULL,
	[RangeName] [nvarchar](max) NULL,
	[Way] [nvarchar](max) NULL,
	[WayName] [nvarchar](max) NULL,
	[OrganizationalUnit] [nvarchar](max) NULL,
	[Organizationunitname] [nvarchar](max) NULL,
	[Clientclass] [nvarchar](max) NULL,
	[SalesCenterCode] [nvarchar](max) NULL,
	[SalesCenterName] [nvarchar](max) NULL,
	[Customertype] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_CacheData] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


