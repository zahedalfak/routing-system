USE [rasharoutesystem]
GO

/****** Object:  Table [dbo].[AdminPermission]    Script Date: 2/28/2020 6:12:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AdminPermission](
	[id] [int] NOT NULL,
	[admin_id] [int] NULL,
	[permission_id] [int] NULL,
 CONSTRAINT [PK_AdminPermission] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AdminPermission]  WITH CHECK ADD  CONSTRAINT [FK_AdminPermission_Admins] FOREIGN KEY([admin_id])
REFERENCES [dbo].[Admins] ([id])
GO

ALTER TABLE [dbo].[AdminPermission] CHECK CONSTRAINT [FK_AdminPermission_Admins]
GO

ALTER TABLE [dbo].[AdminPermission]  WITH CHECK ADD  CONSTRAINT [FK_AdminPermission_Permissions] FOREIGN KEY([permission_id])
REFERENCES [dbo].[Permissions] ([id])
GO

ALTER TABLE [dbo].[AdminPermission] CHECK CONSTRAINT [FK_AdminPermission_Permissions]
GO


