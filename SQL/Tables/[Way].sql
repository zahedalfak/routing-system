USE [rasharoutesystem]
GO

/****** Object:  Table [dbo].[Way]    Script Date: 2/28/2020 6:15:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Way](
	[id] [varchar](5) NOT NULL,
	[name] [nvarchar](100) NULL,
	[rang_id] [varchar](5) NULL,
	[city_id] [int] NULL,
	[base_id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Way] PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[base_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Way]  WITH CHECK ADD  CONSTRAINT [FK_Way_City] FOREIGN KEY([city_id])
REFERENCES [dbo].[City] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Way] CHECK CONSTRAINT [FK_Way_City]
GO


