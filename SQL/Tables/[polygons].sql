USE [rasharoutesystem]
GO

/****** Object:  Table [dbo].[polygons]    Script Date: 2/28/2020 6:15:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[polygons](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [nchar](10) NULL,
	[polygons] [nchar](10) NULL,
	[city] [nchar](10) NULL
) ON [PRIMARY]
GO


