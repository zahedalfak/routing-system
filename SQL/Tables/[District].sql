USE [rasharoutesystem]
GO

/****** Object:  Table [dbo].[District]    Script Date: 2/28/2020 6:14:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[District](
	[id_base] [int] IDENTITY(1,1) NOT NULL,
	[id] [varchar](5) NOT NULL,
	[name] [nvarchar](50) NULL,
	[city_id] [int] NULL,
	[region_id] [varchar](5) NULL,
 CONSTRAINT [PK_District] PRIMARY KEY CLUSTERED 
(
	[id_base] ASC,
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[District]  WITH CHECK ADD  CONSTRAINT [FK_District_City] FOREIGN KEY([city_id])
REFERENCES [dbo].[City] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[District] CHECK CONSTRAINT [FK_District_City]
GO


