USE [rasharoutesystem]
GO

/****** Object:  Table [dbo].[Customers]    Script Date: 2/28/2020 6:13:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Customers](
	[id] [int] NOT NULL,
	[fullname] [nvarchar](50) NULL,
	[business_id] [int] NULL,
	[seller_id] [int] NULL,
	[home_number] [nvarchar](20) NULL,
	[home_number_2] [nvarchar](20) NULL,
	[phone_number] [nvarchar](20) NULL,
	[phone_number_2] [nvarchar](20) NULL,
	[national_code] [nvarchar](15) NULL,
	[postal_code] [nvarchar](15) NULL,
	[economic_code] [nvarchar](20) NULL,
	[address] [nvarchar](250) NULL,
	[lat] [float] NULL,
	[long] [float] NULL,
	[status] [tinyint] NULL,
	[activity_id] [int] NULL,
	[market_id] [int] NULL,
	[create_at_persian] [nvarchar](50) NULL,
	[create_at_england] [date] NULL,
	[city_id] [int] NULL,
	[city_code] [int] NULL,
	[region_id] [varchar](5) NULL,
	[region_code] [int] NULL,
	[distract_id] [varchar](5) NULL,
	[distract_code] [int] NULL,
	[rang_id] [varchar](5) NULL,
	[range_code] [int] NULL,
	[way_id] [varchar](5) NULL,
	[way_code] [int] NULL,
	[classs] [nvarchar](3) NULL,
	[sell_center_code] [int] NULL,
	[sell_center_name] [nvarchar](30) NULL,
	[customer_type_id] [int] NULL,
	[description] [nvarchar](max) NULL,
	[create_at] [datetime] NULL,
	[update_at] [datetime] NULL,
	[Organizational_id] [int] NULL,
	[admin_insert_id] [int] NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [FK_Customers_Activity] FOREIGN KEY([activity_id])
REFERENCES [dbo].[Activity] ([id])
GO

ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [FK_Customers_Activity]
GO

ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [FK_Customers_Admins] FOREIGN KEY([admin_insert_id])
REFERENCES [dbo].[Admins] ([id])
GO

ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [FK_Customers_Admins]
GO

ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [FK_Customers_Business] FOREIGN KEY([business_id])
REFERENCES [dbo].[Business] ([id])
GO

ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [FK_Customers_Business]
GO

ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [FK_Customers_City] FOREIGN KEY([city_id])
REFERENCES [dbo].[City] ([id])
GO

ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [FK_Customers_City]
GO

ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [FK_Customers_CustomerType] FOREIGN KEY([customer_type_id])
REFERENCES [dbo].[CustomerType] ([id])
GO

ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [FK_Customers_CustomerType]
GO

ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [FK_Customers_Market] FOREIGN KEY([market_id])
REFERENCES [dbo].[Market] ([id])
GO

ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [FK_Customers_Market]
GO

ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [FK_Customers_Organizational] FOREIGN KEY([Organizational_id])
REFERENCES [dbo].[Organizational] ([id])
GO

ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [FK_Customers_Organizational]
GO

ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [FK_Customers_SellCenterCode] FOREIGN KEY([sell_center_code])
REFERENCES [dbo].[SellCenterCode] ([id])
GO

ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [FK_Customers_SellCenterCode]
GO

ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [FK_Customers_Seller] FOREIGN KEY([seller_id])
REFERENCES [dbo].[Seller] ([id])
GO

ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [FK_Customers_Seller]
GO


