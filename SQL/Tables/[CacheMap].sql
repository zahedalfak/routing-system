USE [rasharoutesystem]
GO

/****** Object:  Table [dbo].[CacheMap]    Script Date: 2/28/2020 6:13:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CacheMap](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[latlong] [varchar](50) NULL,
	[distance] [float] NULL,
	[customer_id] [int] NULL,
 CONSTRAINT [PK_CacheMap] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


