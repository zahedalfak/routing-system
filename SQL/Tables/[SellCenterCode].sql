USE [rasharoutesystem]
GO

/****** Object:  Table [dbo].[SellCenterCode]    Script Date: 2/28/2020 6:15:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SellCenterCode](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [nvarchar](10) NULL,
 CONSTRAINT [PK_SellCenterCode] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


