USE [master]
GO
/****** Object:  Database [rasharoutesystem]    Script Date: 2/28/2020 6:17:32 PM ******/
CREATE DATABASE [rasharoutesystem]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'rasharoutesystem', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\rasharoutesystem.mdf' , SIZE = 73728KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'rasharoutesystem_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\rasharoutesystem_log.ldf' , SIZE = 204800KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [rasharoutesystem] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [rasharoutesystem].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [rasharoutesystem] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [rasharoutesystem] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [rasharoutesystem] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [rasharoutesystem] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [rasharoutesystem] SET ARITHABORT OFF 
GO
ALTER DATABASE [rasharoutesystem] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [rasharoutesystem] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [rasharoutesystem] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [rasharoutesystem] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [rasharoutesystem] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [rasharoutesystem] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [rasharoutesystem] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [rasharoutesystem] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [rasharoutesystem] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [rasharoutesystem] SET  DISABLE_BROKER 
GO
ALTER DATABASE [rasharoutesystem] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [rasharoutesystem] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [rasharoutesystem] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [rasharoutesystem] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [rasharoutesystem] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [rasharoutesystem] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [rasharoutesystem] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [rasharoutesystem] SET RECOVERY FULL 
GO
ALTER DATABASE [rasharoutesystem] SET  MULTI_USER 
GO
ALTER DATABASE [rasharoutesystem] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [rasharoutesystem] SET DB_CHAINING OFF 
GO
ALTER DATABASE [rasharoutesystem] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [rasharoutesystem] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [rasharoutesystem] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'rasharoutesystem', N'ON'
GO
ALTER DATABASE [rasharoutesystem] SET QUERY_STORE = OFF
GO
USE [rasharoutesystem]
GO
/****** Object:  User [rasharoute]    Script Date: 2/28/2020 6:17:32 PM ******/
CREATE USER [rasharoute] FOR LOGIN [rasharoute] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [rasharoute]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [rasharoute]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [rasharoute]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [rasharoute]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [rasharoute]
GO
ALTER ROLE [db_datareader] ADD MEMBER [rasharoute]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [rasharoute]
GO
ALTER ROLE [db_denydatareader] ADD MEMBER [rasharoute]
GO
ALTER ROLE [db_denydatawriter] ADD MEMBER [rasharoute]
GO
/****** Object:  Table [dbo].[City]    Script Date: 2/28/2020 6:17:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[City](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
 CONSTRAINT [PK_city] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Region]    Script Date: 2/28/2020 6:17:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Region](
	[id] [varchar](5) NOT NULL,
	[name] [nvarchar](50) NULL,
	[city_id] [int] NOT NULL,
	[base_id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Region] PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[base_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[View_1]    Script Date: 2/28/2020 6:17:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_1]
AS
SELECT        dbo.City.id, dbo.City.name
FROM            dbo.City INNER JOIN
                         dbo.Region ON dbo.City.id = dbo.Region.city_id
GROUP BY dbo.City.id, dbo.City.name
GO
/****** Object:  Table [dbo].[Activity]    Script Date: 2/28/2020 6:17:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Activity](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Activity] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AdminPermission]    Script Date: 2/28/2020 6:17:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdminPermission](
	[id] [int] NOT NULL,
	[admin_id] [int] NULL,
	[permission_id] [int] NULL,
 CONSTRAINT [PK_AdminPermission] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Admins]    Script Date: 2/28/2020 6:17:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admins](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](50) NULL,
	[password] [nvarchar](max) NULL,
	[name] [nvarchar](50) NULL,
	[lname] [nvarchar](50) NULL,
	[image] [nvarchar](50) NULL,
 CONSTRAINT [PK_Admins] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Business]    Script Date: 2/28/2020 6:17:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Business](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](70) NULL,
 CONSTRAINT [PK_Business] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CacheData]    Script Date: 2/28/2020 6:17:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CacheData](
	[CustomerID] [varchar](50) NOT NULL,
	[CustomerName] [nvarchar](max) NULL,
	[CustomerBusinessName] [nvarchar](max) NULL,
	[SellerCode] [varchar](50) NULL,
	[SellerName] [nvarchar](max) NULL,
	[PhoneNumber] [varchar](50) NULL,
	[PhoneNumber2] [varchar](50) NULL,
	[MobileNumber] [varchar](50) NULL,
	[NationalCode] [varchar](50) NULL,
	[PostalCode] [varchar](50) NULL,
	[EconomicCode] [varchar](50) NULL,
	[Address] [nvarchar](max) NULL,
	[Longitude] [varchar](50) NULL,
	[Latitude] [varchar](50) NULL,
	[Status] [nvarchar](max) NULL,
	[Activity] [nvarchar](max) NULL,
	[Market] [nvarchar](max) NULL,
	[Openingdate] [nvarchar](max) NULL,
	[Region] [nvarchar](max) NULL,
	[RegionName] [nvarchar](max) NULL,
	[District] [nvarchar](max) NULL,
	[DistrictName] [nvarchar](max) NULL,
	[Range] [nvarchar](max) NULL,
	[RangeName] [nvarchar](max) NULL,
	[Way] [nvarchar](max) NULL,
	[WayName] [nvarchar](max) NULL,
	[OrganizationalUnit] [nvarchar](max) NULL,
	[Organizationunitname] [nvarchar](max) NULL,
	[Clientclass] [nvarchar](max) NULL,
	[SalesCenterCode] [nvarchar](max) NULL,
	[SalesCenterName] [nvarchar](max) NULL,
	[Customertype] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_CacheData] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CacheMap]    Script Date: 2/28/2020 6:17:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CacheMap](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[latlong] [varchar](50) NULL,
	[distance] [float] NULL,
	[customer_id] [int] NULL,
 CONSTRAINT [PK_CacheMap] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomerType]    Script Date: 2/28/2020 6:17:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](20) NULL,
 CONSTRAINT [PK_CustomerType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 2/28/2020 6:17:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[id] [int] NOT NULL,
	[fullname] [nvarchar](50) NULL,
	[business_id] [int] NULL,
	[seller_id] [int] NULL,
	[home_number] [nvarchar](20) NULL,
	[home_number_2] [nvarchar](20) NULL,
	[phone_number] [nvarchar](20) NULL,
	[phone_number_2] [nvarchar](20) NULL,
	[national_code] [nvarchar](15) NULL,
	[postal_code] [nvarchar](15) NULL,
	[economic_code] [nvarchar](20) NULL,
	[address] [nvarchar](250) NULL,
	[lat] [float] NULL,
	[long] [float] NULL,
	[status] [tinyint] NULL,
	[activity_id] [int] NULL,
	[market_id] [int] NULL,
	[create_at_persian] [nvarchar](50) NULL,
	[create_at_england] [date] NULL,
	[city_id] [int] NULL,
	[city_code] [int] NULL,
	[region_id] [varchar](5) NULL,
	[region_code] [int] NULL,
	[distract_id] [varchar](5) NULL,
	[distract_code] [int] NULL,
	[rang_id] [varchar](5) NULL,
	[range_code] [int] NULL,
	[way_id] [varchar](5) NULL,
	[way_code] [int] NULL,
	[classs] [nvarchar](3) NULL,
	[sell_center_code] [int] NULL,
	[sell_center_name] [nvarchar](30) NULL,
	[customer_type_id] [int] NULL,
	[description] [nvarchar](max) NULL,
	[create_at] [datetime] NULL,
	[update_at] [datetime] NULL,
	[Organizational_id] [int] NULL,
	[admin_insert_id] [int] NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[District]    Script Date: 2/28/2020 6:17:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[District](
	[id_base] [int] IDENTITY(1,1) NOT NULL,
	[id] [varchar](5) NOT NULL,
	[name] [nvarchar](50) NULL,
	[city_id] [int] NULL,
	[region_id] [varchar](5) NULL,
 CONSTRAINT [PK_District] PRIMARY KEY CLUSTERED 
(
	[id_base] ASC,
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Market]    Script Date: 2/28/2020 6:17:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Market](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Market] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Organizational]    Script Date: 2/28/2020 6:17:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Organizational](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](100) NULL,
 CONSTRAINT [PK_Organizational] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Permissions]    Script Date: 2/28/2020 6:17:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permissions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Permissions] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Range]    Script Date: 2/28/2020 6:17:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Range](
	[id] [varchar](5) NOT NULL,
	[name] [nvarchar](50) NULL,
	[district_id] [varchar](5) NULL,
	[city_id] [int] NULL,
	[base_id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Range] PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[base_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SellCenterCode]    Script Date: 2/28/2020 6:17:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SellCenterCode](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [nvarchar](10) NULL,
 CONSTRAINT [PK_SellCenterCode] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Seller]    Script Date: 2/28/2020 6:17:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Seller](
	[id] [int] NOT NULL,
	[name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Seller] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Way]    Script Date: 2/28/2020 6:17:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Way](
	[id] [varchar](5) NOT NULL,
	[name] [nvarchar](100) NULL,
	[rang_id] [varchar](5) NULL,
	[city_id] [int] NULL,
	[base_id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Way] PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[base_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[polygons]    Script Date: 2/28/2020 6:17:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[polygons](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [nchar](10) NULL,
	[polygons] [nchar](10) NULL,
	[city] [nchar](10) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AdminPermission]  WITH CHECK ADD  CONSTRAINT [FK_AdminPermission_Admins] FOREIGN KEY([admin_id])
REFERENCES [dbo].[Admins] ([id])
GO
ALTER TABLE [dbo].[AdminPermission] CHECK CONSTRAINT [FK_AdminPermission_Admins]
GO
ALTER TABLE [dbo].[AdminPermission]  WITH CHECK ADD  CONSTRAINT [FK_AdminPermission_Permissions] FOREIGN KEY([permission_id])
REFERENCES [dbo].[Permissions] ([id])
GO
ALTER TABLE [dbo].[AdminPermission] CHECK CONSTRAINT [FK_AdminPermission_Permissions]
GO
ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [FK_Customers_Activity] FOREIGN KEY([activity_id])
REFERENCES [dbo].[Activity] ([id])
GO
ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [FK_Customers_Activity]
GO
ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [FK_Customers_Admins] FOREIGN KEY([admin_insert_id])
REFERENCES [dbo].[Admins] ([id])
GO
ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [FK_Customers_Admins]
GO
ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [FK_Customers_Business] FOREIGN KEY([business_id])
REFERENCES [dbo].[Business] ([id])
GO
ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [FK_Customers_Business]
GO
ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [FK_Customers_City] FOREIGN KEY([city_id])
REFERENCES [dbo].[City] ([id])
GO
ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [FK_Customers_City]
GO
ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [FK_Customers_CustomerType] FOREIGN KEY([customer_type_id])
REFERENCES [dbo].[CustomerType] ([id])
GO
ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [FK_Customers_CustomerType]
GO
ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [FK_Customers_Market] FOREIGN KEY([market_id])
REFERENCES [dbo].[Market] ([id])
GO
ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [FK_Customers_Market]
GO
ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [FK_Customers_Organizational] FOREIGN KEY([Organizational_id])
REFERENCES [dbo].[Organizational] ([id])
GO
ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [FK_Customers_Organizational]
GO
ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [FK_Customers_SellCenterCode] FOREIGN KEY([sell_center_code])
REFERENCES [dbo].[SellCenterCode] ([id])
GO
ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [FK_Customers_SellCenterCode]
GO
ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [FK_Customers_Seller] FOREIGN KEY([seller_id])
REFERENCES [dbo].[Seller] ([id])
GO
ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [FK_Customers_Seller]
GO
ALTER TABLE [dbo].[District]  WITH CHECK ADD  CONSTRAINT [FK_District_City] FOREIGN KEY([city_id])
REFERENCES [dbo].[City] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[District] CHECK CONSTRAINT [FK_District_City]
GO
ALTER TABLE [dbo].[Range]  WITH CHECK ADD  CONSTRAINT [FK_Range_City] FOREIGN KEY([city_id])
REFERENCES [dbo].[City] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Range] CHECK CONSTRAINT [FK_Range_City]
GO
ALTER TABLE [dbo].[Region]  WITH CHECK ADD  CONSTRAINT [FK_Region_City] FOREIGN KEY([city_id])
REFERENCES [dbo].[City] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Region] CHECK CONSTRAINT [FK_Region_City]
GO
ALTER TABLE [dbo].[Way]  WITH CHECK ADD  CONSTRAINT [FK_Way_City] FOREIGN KEY([city_id])
REFERENCES [dbo].[City] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Way] CHECK CONSTRAINT [FK_Way_City]
GO
/****** Object:  StoredProcedure [dbo].[DeleteCatchData]    Script Date: 2/28/2020 6:17:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteCatchData]
	
AS
BEGIN
	DELETE CacheData
END
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[70] 4[12] 2[10] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "City"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 113
               Right = 210
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Region"
            Begin Extent = 
               Top = 200
               Left = 192
               Bottom = 330
               Right = 362
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_1'
GO
USE [master]
GO
ALTER DATABASE [rasharoutesystem] SET  READ_WRITE 
GO
